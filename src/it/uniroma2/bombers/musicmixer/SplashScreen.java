package it.uniroma2.bombers.musicmixer;

import it.uniroma2.bombers.musicmixer.gui.FragmentSettings;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Window;
import android.view.WindowManager;

public class SplashScreen extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash_screen);
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Long first = System.currentTimeMillis();
				
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(SplashScreen.this);
				String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
				String recPath = pref.getString(getString(R.string.pref_key_recorder_folder), FragmentSettings.DEF_PATH_REC);
				
				File pl = new File(plPath);
				File rec = new File(recPath);
				if (!pl.isDirectory())
					pl.mkdirs();
				if (!rec.isDirectory())
					rec.mkdirs();
				
				PlayListManager.refreshPlaylist(plPath);
				
				Long last = System.currentTimeMillis();
				
				if (last - first < 2000) {
					try {
						Thread.sleep(2000 - last + first);
					} catch (InterruptedException e) {
					}
				}
				
				SplashScreen.this.startActivity(new Intent(SplashScreen.this, MainActivity.class));
				SplashScreen.this.finish();
				
			}
		}).start();
	}
}
