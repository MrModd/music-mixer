package it.uniroma2.bombers.musicmixer;

import it.uniroma2.bombers.musicmixer.gui.FragmentSettings;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class RecorderService extends Service{

	private static int FOREGROUND_ID=2236;
	public static final String recorder = "RecorderService";
	
	public final IBinder loc=new LocalBinder();
	private MediaRecorder mr = null;
	private int outputFormat = 0;
	private String path = null;
	private Context ctx;
	private boolean running = false;
	
	@Override
	public int onStartCommand(Intent i, int flags, int startId){
		super.onStartCommand(i, flags, startId);
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		path = pref.getString(getString(R.string.pref_key_recorder_folder), FragmentSettings.DEF_PATH_REC);
		Log.i(recorder, "Path for records: "+path);
		
		return START_STICKY;
	}
	
	@Override
	public void onDestroy (){
		if(status()){
			mr.stop();
			mr.reset();
			mr.release();
			running = false;
		}
		super.onDestroy();
	}
	
	public class LocalBinder extends Binder {
        public RecorderService getService() {
            return RecorderService.this;
        }
    }

	@Override
	public IBinder onBind(Intent intent) {
		return loc;
	}
	
	/**
	 * Start recording function
	 * @param ctx
	 * @throws IOException
	 */
	public void startRec(Context ctx) throws IOException{
		if(!running){
			this.ctx = ctx;
			mr = new MediaRecorder();
			mr.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		
			outputFormat = MediaRecorder.OutputFormat.THREE_GPP;
		
			if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
				throw new IOException("External storage unmounted/invalid");
			}
			//The output file name is a concatenation of time and date of the recording
			Calendar date = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HHmmss_dd-MM-yyyy", Locale.getDefault());
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			path = pref.getString(getString(R.string.pref_key_recorder_folder), FragmentSettings.DEF_PATH_REC);
			path = path.concat("/"+sdf.format(date.getTime())+".3gp");
		
			File output = new File(path);
			if (!output.createNewFile()){
				throw new IOException("Failure during createNewFile()");
			}
			
			mr.setOutputFormat(outputFormat);
			mr.setOutputFile(this.path);
			mr.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
		
			mr.prepare();
			mr.start();
		
			startForeground(FOREGROUND_ID,
                buildForegroundNotification());
			running = true;
		}
	}
	
	/**
	 * Stop recording function
	 */
	public void stopRec(){
		if(running){		
			stopForeground(true);
		
			mr.stop();
			mr.reset();
			mr.release();
			mr = null;
			running = false;
		}
	}
	
	private Notification buildForegroundNotification() {
	    NotificationCompat.Builder b=new NotificationCompat.Builder(this);

	    b.setOngoing(true);

	    b.setContentTitle(getResources().getText(R.string.notification_title))
	     .setContentText(getResources().getText(R.string.recording_notification_text))
	     .setSmallIcon(R.drawable.ic_launcher)
	     .setTicker(getResources().getText(R.string.recording_notification_ticker));
	    
	    //Creating intent to call when notification is clicked
	    Intent i = new Intent(ctx, MainActivity.class);
	    PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);
	    b.setContentIntent(pi);

	    return(b.build());
	  }

	/**
	 * Function to get if the recorder is running
	 * @return true if the recorder's running, false otherwise
	 */
	public boolean status(){
		return running;
	}
}