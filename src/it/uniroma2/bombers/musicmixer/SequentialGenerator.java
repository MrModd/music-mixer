package it.uniroma2.bombers.musicmixer;

/**
 * This class implements Generator interface methods
 * in order to generate sequential numbers starting from 
 * 0 to a MAX number specified in the constructor.
 *
 * @author Giulio Picierro
 */
public class SequentialGenerator implements Generator {
	
	private final int MAX;
	private int cur;
	
	public SequentialGenerator(int max) {
		this.MAX = max;
		this.cur = 0;
	}

	@Override
	public int get() {
		return Math.min(cur, MAX);
	}

	@Override
	public int next() {
		cur = Math.min(MAX, cur+1);
		
		return cur;
	}

	@Override
	public int prev() {
		cur = Math.max(0, cur-1);
		
		return cur;
	}

	@Override
	public void set(int id) {
		if (id < 0 || id >= MAX)
			throw new IllegalArgumentException();
		
		cur = id;
	}

	@Override
	public void reset() {
		cur = 0;
	}

	@Override
	public final int max() {
		return MAX;
	}
}