
package it.uniroma2.bombers.musicmixer;

/**
 * This interface will expose methods to
 * generate integers on-the-fly according to 
 * some method specified by the implementation
 *
 * @author Giulio Picierro
 */
public interface Generator {
	
	/**
	 * Retreive the last generated number
	 * @return the current number
	 */
	public int get();
	
	/**
	 * Get the next number according to generation policy
	 * described by implementation
	 *
	 * @return the next number
	 */
	public int next();
	
	/**
	 * Retreive the previously generated number
	 *
	 * @return the previous number
	 */
	public int prev();
	
	/**
	 * Override the sequence generation, starting from the passed parameter
	 *
	 * @param id The number from which start the generation
	 */
	public void set(int id);
	
	/**
	 * Restart the generator<br />
	 *
	 * Should have the same effect of calling
	 * <code>Generator gen = new GeneratorImpl(..);</code>
	 *
	 */
	public void reset();
	
	/**
	 * Retreive the maximum number the generator could generate
	 * @return The max number that the generator could generate
	 */
	public int max();
}
