package it.uniroma2.bombers.musicmixer;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.*;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * The PlaybackService class expose methods to
 * manage and reproduce audio files in form of a playlist.
 * <br />
 * 
 * @author Bombers Team
 *
 */
public class PlaybackService extends Service
							 implements MediaPlayer.OnPreparedListener,
							 			MediaPlayer.OnCompletionListener,
							 			MediaPlayer.OnErrorListener {
	
	static final String TAG = "PlaybackService";
	static final int NOTIFICATION_ID = 1;
	
	protected MediaPlayer player;
	protected List<String> playlist;
	protected boolean playerPaused;
	protected boolean shuffleEnabled;
	protected boolean playerRunning;
	protected int fadeTime;
	protected int loopsRemaining;
	protected AtomicReference<Generator> generator;
	protected ScheduledExecutorService executor;
	protected ScheduledFuture<?> fadeOutScheduled;
	protected OnChangeListener changeListener;
	protected OnErrorListener errorListener;

	private final Runnable fadeOutRunnable = new Runnable() {

		@Override
		public void run() {

			float volume = 1;

			int start = player.getCurrentPosition();
			int delta = player.getDuration() - start;

			Log.i(TAG, "FadeOut, delta: " + delta);

			while (player.isPlaying()) {

				volume = 1 - (player.getCurrentPosition() - start)/(float)delta;

				Log.i(TAG, "FadeOut Volume: " + volume);

				player.setVolume(volume, volume);

				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					Log.e(TAG, e.getMessage());
					break;
				}
			}

			Log.i(TAG, "FadeOut Stopped");
		}
	};

	public class PlaybackBinder extends Binder {
		
		public PlaybackService getService() {
			return PlaybackService.this;
		}
	}

	public interface OnChangeListener {
		public void onChange(int id);
	}

	public interface OnErrorListener {
		public void onError(int what, int extra);
	}

	/**
	 * Overriding the Service onBind call as
	 * Android requirement to binding with this
	 * service when calling Activity.bindService() method.
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		return new PlaybackBinder();
	}
	
	/**
	 * Overriding the Service onCreate call as
	 * Android requirement.<br />
	 * This actually perform class initialization and act like
	 * a constructor.
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		
		player = new MediaPlayer();
		playlist = new ArrayList<>();

		/*
		 * Useful to generate sequence numbers that will be
		 * played.
		 * Actually only 2 generators are provided:<br/>
		 * @see SequentialGenerator
		 * @see UniqueRandomGenerator
		 * <br />
		 * It will be swapped when setShuffle function is called.
		 */
		Generator default_generator = new SequentialGenerator(playlist.size());

		generator = new AtomicReference<>(default_generator);

		playerPaused = false;
		playerRunning = false;
		shuffleEnabled = false;
		fadeTime = 0;
		loopsRemaining = 0;
		
		executor = new ScheduledThreadPoolExecutor(1);
		fadeOutScheduled = null;

		changeListener = null;
		errorListener = null;

		player.setOnPreparedListener(this);
		player.setOnCompletionListener(this);
		player.setOnErrorListener(this);
		
		Log.i(TAG, "Service created");
	}
	
	/**
	 * Overriding the Service onStartCommand call as
	 * Android requirement. This function will be called
	 * when calling Activity.startService() is performed.
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	/**
	 * This function will schedule fade-out, according to
	 * fadetime and current seek position
	 */
	private void scheduleFadeOut() {
		if (fadeTime > 0) {
			int delay = player.getDuration() - player.getCurrentPosition() - fadeTime;

			Log.i(TAG, "FadeOut delay: " + delay);

			fadeOutScheduled = executor.schedule(fadeOutRunnable, delay, TimeUnit.MILLISECONDS);
		}
	}

	/**
	 * Helper function to get the path of a song given its id
	 * @param id The position of song in playlist
	 * @return The path of the song
	 */
	protected String get(int id) {

		if (id < 0 || id >= playlist.size()) {
			return null;
		}

		String next = playlist.get(id);

		return next;
	}

	/**
	 * Returns the current index on the playlist
	 * @return the current index on the playlist
	 */
	public int getCurrentIndex() {
		return generator.get().get();
	}

	/**
	 * Returns a copy of the current playlist
	 * @return a List that is a copy of the current playlist
	 */
	public List<String> getPlaylist() {
		return new ArrayList<String>(playlist);
	}

	/**
	 * Change the current playlist and
	 * reset generator accordingly<br />
	 * @param playlist The playlist to be reproduced by the service
	 */
	public void setPlaylist(List<String> playlist) {

		stop();

		this.playlist = new ArrayList<>(playlist);

		try {
			Constructor<?> generatorFactory = generator.get().getClass().getConstructor(int.class);

			Generator current_generator = (Generator) generatorFactory.newInstance(playlist.size());

			generator.set(current_generator);
		}
		catch (Exception e) {
			throw new RuntimeException("Cannot instance Generator: " + e.getMessage());
		}
	}
	
	/**
	 * Perform the playback of the playlist or resume the last paused track
	 * @return The id of song being played
	 */
	public int play() {
		
		if (playerPaused) {
			playerPaused = false;
			player.start();
			playerRunning = true;

			scheduleFadeOut();

			int id = generator.get().get();

			Log.i(TAG, "Resumed id " + id);

			return id;
		}

		Generator g = generator.get();

		if (g.get() == g.max()) {
			g.reset();
		}

		return play(generator.get().get());
	}
	
	/**
	 * Perform the playback starting from playlist element id
	 * @param id - the position of element in the playlist
	 * @return The id of song being played
	 */
	public int play(int id) {

		String next = get(id);
		
		if (next == null) return -1;
		
		Log.i(TAG, "Selected to playback: " + next);
		
		generator.get().set(id);
		
		/**
		 * Preparing notification that will show the
		 * song path that is playing.
		 */
		Context ctx = getApplicationContext();

		PendingIntent pi = PendingIntent.getActivity(ctx, 0, new Intent(ctx, MainActivity.class), 0);

		NotificationCompat.Builder bld = new NotificationCompat.Builder(this);

		bld.setSmallIcon(R.drawable.ic_launcher);
		bld.setContentTitle(getString(R.string.notification_title));
		bld.setContentText(getString(R.string.playback_notification_text) + " " + Uri.parse(next).getLastPathSegment());
		bld.setOngoing(true);
		bld.setContentIntent(pi);

		// Prepare the mediaplayer to reproduce the requested song
		try {
			stop();
			player.setDataSource(next);
			player.setAudioStreamType(AudioManager.STREAM_MUSIC);
			player.prepareAsync();
			startForeground(NOTIFICATION_ID, bld.build());
		} catch (IOException e) {
			
			Log.e(TAG, e.getMessage());
			
			int nextId = generator.get().next();

			play(nextId);
		}

		playerRunning = true;

		return id;
	}
	
	/**
	 * Perform playback of the next item in playlist or,
	 * if shuffle is on, select random.
	 * @return The id of the next song that will be played or MAX if
	 * no more songs will be played.
	 */
	public int next() {

		int next = generator.get().next();

		if (next == generator.get().max()) {
			stop();
		}
		else {
			play(next);
		}

		return next;
	}
	
	/**
	 * Perform playback of the previous item in the playlist
	 * @return The id of the song that will be played
	 */
	public int prev() {

		int prev = generator.get().prev();

		play(prev);

		return prev;
	}
	
	/**
	 * Stop the playback of current item. <br />
	 * If the current item was paused, then it will remove the pause state.
	 *
	 */
	public void stop() {
		playerPaused = false;
		playerRunning = false;
		
		player.reset();

		if (fadeOutScheduled != null) {
			fadeOutScheduled.cancel(true);
		}

		stopForeground(true);

		Log.i(TAG, "Stopped id " + generator.get().get());
	}
	
	/**
	 * Pause the playback of current item. <br />
	 * To resume use the play() method (without arguments).
	 * 
	 */
	public int pause() {
		
		if (player.isPlaying()) {
			player.pause();
			
			playerPaused = true;
			playerRunning = false;

			if (fadeOutScheduled != null) {
				fadeOutScheduled.cancel(true);
			}

			int id = generator.get().get();

			Log.i(TAG, "Paused id "  + id);

			return id;
		}

		return -1;
	}
	
	/**
	 * Get player playback status
	 * @return true if the player has a song in playback or false otherwise
	 */
	public boolean isPlaying() {
		return playerRunning;
	}

	/**
	 * Get player pause status
	 * @return true if the player was paused, false otherwise
	 */
	public boolean isPaused() {
		return playerPaused;
	}

	/**
	 * Set how many times the playlist should reproduce.
	 *
	 * @param loops The number of loops: 0 to disable looping, >0 to enable
	 *
	 */
	public void setPlaylistLoops(int loops) {

		Log.i(TAG, "setLoops from: " + loopsRemaining + ", to: " + loops);

		loopsRemaining = loops;
	}

	/**
	 * Enable or disable shuffle on parameter basis
	 *
	 * @param toggle true to enable, false to disable shuffle
	 */
	public void setShuffle(boolean toggle) {

		Log.i(TAG, "setShuffle from: " + shuffleEnabled + ", to: " + toggle);

		if (shuffleEnabled != toggle) {

			// Remember the current song in order to reflect this to new generator
			int id = generator.get().get();

			if (toggle) {
				UniqueRandomGenerator gen = new UniqueRandomGenerator(playlist.size());

				// Set the current song to be the first of new random sequence
				gen.setTop(id);

				generator.set(gen);
			}
			else {
				generator.set(new SequentialGenerator(playlist.size()));
				if (id < generator.get().max()) {
					generator.get().set(id);
					Log.i(TAG, "switch back to sequential generator with song id: " + id);
				}
			}

			shuffleEnabled = toggle;
		}
	}

	/**
	 * Set fade-in/fade-out time
	 *
	 * @param seconds 0 to disable, >0 to enable
	 */
	public void setFade(int seconds) {
		if (seconds < 0) {
			throw new IllegalArgumentException("seconds must be >= 0");
		}

		Log.i(TAG, "setFade from: " + fadeTime + ", to: " + seconds*1000);

		fadeTime = seconds*1000;

		if (fadeOutScheduled != null) {
			fadeOutScheduled.cancel(true);
		}

		if (player.isPlaying()) {
			scheduleFadeOut();
		}
	}

	/**
	 * Gets the duration of current song
	 * @return the duration in milliseconds, or 0 if the player is stopped
	 */
	public int getDuration() {
		if (player.isPlaying() || playerPaused) {
			return player.getDuration();
		}

		return 0;
	}

	/**
	 * Gets the current playback position
	 *
	 * @return the current position in milliseconds, or 0 if the player is stopped
	 */
	public int getPosition() {

		if (player.isPlaying() || playerPaused) {
			return player.getCurrentPosition();
		}

		return 0;
	}

	/**
	 * Seeks to specified time position
	 * @param msec the offset in milliseconds to seek to
	 */
	public void setPosition(int msec) {

		if (player.isPlaying() || playerPaused) {

			int duration = player.getDuration();

			if (msec <= duration) {
				player.seekTo(msec);

				if (fadeOutScheduled != null) {
					fadeOutScheduled.cancel(true);
				}

				if (!playerPaused) {
					scheduleFadeOut();
				}
			}
		}
	}

	/**
	 * Overriding the Service onDestroy call as
	 * Android to perform cleanup of resources.<br />
	 * Actually this method act like a destructor.
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		player.release();
		player = null;
		playlist = null;
		
		Log.i(TAG, "Service destroyed");
	}

	/**
	 * This method will be called asynchronusly when
	 * MediaPlayer is ready for playback.<br />
	 * The current song will be reproduced and if needed fade-in thread
	 * will start, and fade-out thread will be scheduled.
	 */
	@Override
	public void onPrepared(MediaPlayer mp) {

		player.start();
		
		if (fadeTime > 0) {
			player.setVolume(0, 0);

			new Thread( new Runnable() {

				@Override
				public void run() {

					float volume = 0;

					/* FADE-IN CYCLE
					 * The fade cycle will increase volume step by step
					 * according to a function of current time and of
					 * required fade time.
					 *
					 * Volume = (t/fadeTime)^2
					 *
					 * The thread will automatically stop if the user
					 * change the song or stop the player.
					 */
					while (player.isPlaying() && volume < 1) {

						volume = player.getCurrentPosition()/(float)fadeTime;

						volume *= volume;

						Log.i(TAG, "FadeIn Volume: " + volume);

						player.setVolume(volume, volume);

						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							Log.e(TAG, "onPrepared: " + e.getMessage());
						}
					}

					Log.i(TAG, "FadeIn Stopped.");
				}
			}).start();

			scheduleFadeOut();
		}

		Log.i(TAG, "Started playback of id " + generator.get().get());
	}

	/**
	 * Set listener to receive notification on song changed<br />
	 * @param listener
	 */
	public void setOnChangeListener(OnChangeListener listener) {
		this.changeListener = listener;
	}

	/**
	 * Set listener to receive notification on error<br />
	 * @param listener
	 */
	public void setOnErrorListener(OnErrorListener listener) {
		this.errorListener = listener;
	}

	/**
	 * This method will be called when the current song
	 * reach the end.<br />
	 * If the playlist not reach the end, the next song will be
	 * prepared for playback and the listener will be triggered to
	 * notify the observer about the song change.
	 */
	@Override
	public void onCompletion(MediaPlayer mp) {
		if (next() == generator.get().max() && loopsRemaining > 0) {

			generator.get().reset();

			play();

			loopsRemaining--;

			Log.i(TAG, "Loops remaining: " + loopsRemaining);
		}

		if (changeListener != null) {
			changeListener.onChange(generator.get().get());
		}
	}
	
	/**
	 * Get the audio session id of current media player<br />
	 * Can be useful if for example you want to use audiofx classes or
	 * visualizer
	 * @return the current audio session id
	 */
	public int getAudioSessionId(){
		return player.getAudioSessionId();
	}

	/**
	 * This method will be called when MediaPlayer triggers
	 * an error. A listener will be called to notify the observer about
	 * the error.
	 *
	 * @see MediaPlayer documentation to read more about the what and extra
	 * parameters of this function
	 */
	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		
		stop();
		
		if (errorListener != null) {
			errorListener.onError(what, extra);
		}

		return true;
	}
}
