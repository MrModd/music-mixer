package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.R;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FileAdapter extends BaseAdapter {

	private Activity activity;
    private static LayoutInflater inflater=null;
    private ArrayList<File> data;
 
    public FileAdapter(Activity a, ArrayList<File> data) {
        activity = a;
        this.data =  data;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    public int getCount() {
        return data.size();
    }
 
    public Object getItem(int position) {
        return position;
    }
 
    public long getItemId(int position) {
        return position;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.file_adapter, null);
 
        TextView name = (TextView)vi.findViewById(R.id.file_adapter_name);				//file name
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.file_adapter_thumbnail);	// thumb image
        
        File item = data.get(position);
 
        //If directory, set directory icon. If file, set file icon
        if(item.isDirectory()){
        	thumb_image.setImageResource(R.drawable.folder_icon);
        	name.setText(item.getName()+"/");
        }
        else{
        	thumb_image.setImageResource(R.drawable.file_icon);
        	name.setText(item.getName());
        }
        return vi;
    }

	

}
