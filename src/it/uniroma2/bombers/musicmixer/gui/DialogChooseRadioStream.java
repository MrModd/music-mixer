package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class DialogChooseRadioStream extends DialogFragment {
	public static final String HISTORY_FILE = "history.dat";

	private ArrayList<String> history = null;
	private Integer historySize;
	private AutoCompleteTextView inputText;
	private ArrayAdapter<String> adapter;
	
    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;
	
	/* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String url);
        public void onDialogNegativeClick(DialogFragment dialog);
    }
	
	public void setOnDialogClickListener(NoticeDialogListener listener) {
		mListener = listener;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();
		
		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		View v = inflater.inflate(R.layout.dialog_choose_radio_stream, null);
		builder.setView(v);
		
		inputText = (AutoCompleteTextView) v.findViewById(R.id.dialog_radio_stream_url);
		
		//Get history size
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		historySize = Integer.valueOf(
				pref.getString(getString(R.string.pref_key_radio_history_length),
				getResources().getString(R.string.pref_radio_history_length_default)));

		//Add elements in the dialog box
		builder.setTitle(R.string.dialog_radio_stream_title);
		builder.setPositiveButton(R.string.dialog_radio_Stream_ok_button, new OnClickListener () {
		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mListener != null && inputText.length() > 0) {
					
					String url = inputText.getText().toString();
					if (!historyContains(url))
						history.add(url);
					while (history.size() > historySize) {
						history.remove(0);
					}
					
					try {
						File outFile = new File(getActivity().getCacheDir(), HISTORY_FILE);
						FileOutputStream outStream = new FileOutputStream(outFile);
						ObjectOutputStream outObj = new ObjectOutputStream(outStream);
						outObj.writeObject(history);
						outObj.close();
						outStream.close();
					} catch (IOException e) {
						//Do nothing
					}
					
					mListener.onDialogPositiveClick(DialogChooseRadioStream.this, inputText.getText().toString());
				}
			}
			
		});
		builder.setNegativeButton(R.string.dialog_radio_Stream_cancel_button, new OnClickListener () {
		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mListener != null) {
					mListener.onDialogNegativeClick(DialogChooseRadioStream.this);
				}
			}
			
		});
		
		if (history == null) {
			//Try to deserialize
			File inFile = new File(getActivity().getCacheDir(), HISTORY_FILE);
			try {
				FileInputStream inStream = new FileInputStream(inFile);
				ObjectInputStream inObj = new ObjectInputStream(inStream);
				history = (ArrayList<String>)inObj.readObject();
				inObj.close();
				inStream.close();
			} catch (IOException | ClassNotFoundException e) {
				//File doesn't exist
				history = new ArrayList<String>();
			}
		}
		
		adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, history.toArray(new String[1]));
		inputText.setAdapter(adapter);
		return builder.create();
	}
	
	private boolean historyContains(String url) {
		if (history != null) {
			for (int i = 0; i < history.size(); i++) {
				if (history.get(i).compareTo(url) == 0) {
					return true;
				}
			}
		}
		return false;
	}

}
