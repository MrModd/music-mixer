package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.Id3Tag;
import it.uniroma2.bombers.musicmixer.PlayListManager;
import it.uniroma2.bombers.musicmixer.R;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;

/**
 * This class implements an Android Activity that show to the user
 * a list of current songs in a given playlist.
 * The activity should be triggered with an Intent that can contain the name
 * of playlist to load and to manage. If the Intent extra is null the playlist
 * is empty, and act like a new one to be created.
 *
 * @author Bombers Team
 */
public class ActivityManageSongsInPlaylist extends Activity implements MediaPlayer.OnPreparedListener {
	
	public static final String EXTRA_PLAYLIST_NAME = "playlist";
	public static final String EXTRA_NEW_PLAYLIST = "new";
	public static final String NEW_PLAYLIST_NAME = "new_playlist";
	public static final Integer REQUEST_CODE_FILE_PICKER = 42;
	public static final String DEFAULT_PREVIEW_TIME = "5"; // seconds
	public static final String TAG = "ActivityManageSongInPlaylist";
	
	private Intent intent;
	private String playlistName;
	
	private ListView list;
	private ProgressBar progress;
	private SongAdapter listAdapter;
	private ArrayList<String> songsPath;
	ArrayList<HashMap<String, String>> playlist;
	
	private volatile AsyncTask<Void, Void, Void> refreshListTask;

	private MediaPlayer player;

	private int previewIndex = -1;
	
	private ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
	
	private ScheduledFuture<?> previewHandle = null;
	
	/**
	 * This Runnable will be scheduled for execution in order
	 * to stop the preview when needed, and update UI status
	 * accordingly (ie. removing playback symbol from current item).
	 * @see onPrepared
	 */
	private Runnable stopPlayerPreview = new Runnable() {
		
		@Override
		public void run() {
			if (player.isPlaying()) {
				player.stop();
				Log.i(TAG, "Preview Stopped");

				// Remove playback symbol in the list item
				HashMap<String, String> song = playlist.get(previewIndex);

				song.put(SongAdapter.KEY_PLAYING, "");

				previewIndex = -1;

				ActivityManageSongsInPlaylist.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						listAdapter.notifyDataSetChanged();
					}
				});
			}
		}
	};
	
	/**
	 * Called when the activity is created.<br />
	 * This method will act like a constructor for the activity.<br />
	 * Will handle the Intent in order to load the current playlist to manage.
	 * It will also start an @link Asynctask to populate the list adapter if
	 * needed.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_list_view);
		
		list = (ListView)findViewById(R.id.activityListView);
		progress = (ProgressBar)findViewById(R.id.progressBar);
		playlist = new ArrayList<HashMap<String, String>>();
		listAdapter = new SongAdapter(this, playlist);
		
		list.setAdapter(listAdapter);

		refreshListTask = new refreshRawList();
		
		intent = getIntent();
		if (intent.getBooleanExtra(EXTRA_NEW_PLAYLIST, false)) {
			playlistName = findNewPlaylistName();
		}
		else {
			playlistName = intent.getStringExtra(EXTRA_PLAYLIST_NAME);
			if (playlistName == null) {
				playlistName = findNewPlaylistName();
			}
		}
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
		PlayListManager.createPlaylist(plPath, playlistName);
		
		setTitle(playlistName);
		
		player = new MediaPlayer();
		player.setOnPreparedListener(this);

		refreshList();

		Log.i(TAG, "Created");
	}

	/**
	 * Called when the activity starts.<br />
	 * It will set two listeners for the item list:<br />
	 * 1) One click song preview<br />
	 * 2) Long click to remove a song<br />
	 */
	@Override
	protected void onStart() {
		super.onStart();
		
		// 1) Setting song preview Listener
		list.setOnItemClickListener( new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int pos,
					long id) {
				
				if (previewHandle != null) {
					previewHandle.cancel(true);
				}
				
				String path = songsPath.get(pos);
				
				player.reset();

				if (pos != previewIndex) {
					// song switch
					if (previewIndex != -1) {
						// unset previous
						HashMap<String, String> song = playlist.get(previewIndex);

						// hide play icon in the view
						song.put(SongAdapter.KEY_PLAYING, "");
					}

					// update the index of current preview item
					previewIndex = pos;
				}

				/*
				 * Preparing MediaPlayer to playback the selected item.
				 * When the player is ready it will asynchronously call
				 * @link onPrepared function.
				 */
				try {
					player.setDataSource(path);
					player.setAudioStreamType(AudioManager.STREAM_MUSIC);
					player.prepareAsync();
				} catch (IOException e) {
					Log.e("ActivityManagerSongsInPlaylist", e.getMessage());
				}
			}
		});
		
		// 2) Setting remove song Listener
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				final int index = position;
				
				// Create a 2-choice Dialog (Ok, Cancel)
				AlertDialog.Builder builder = new AlertDialog.Builder(ActivityManageSongsInPlaylist.this);
				builder.setTitle(playlist.get(position).get(SongAdapter.KEY_TITLE))
					.setMessage(R.string.dialog_delete_song_message)
					.setPositiveButton(R.string.dialog_delete_song_ok_button, new OnClickListener() {
						
						/**
						 * Handler for OK button press<br />
						 * This will update the underlaying playlist file and
						 * also update the ListView adapter to refresh the UI.
						 * @see removeSongFromList
						 */
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
							SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ActivityManageSongsInPlaylist.this);
							String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
							PlayListManager.removeSong(index, plPath, playlistName);

							removeSongFromList(index);
							
						}
					})
					.setNegativeButton(R.string.dialog_delete_song_cancel_button, new OnClickListener() {
						
						/**
						 * Handler for CANCEL button press<br />
						 * Do nothing.
						 */
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					}).create().show();
				return true;
			}
			
		});
	}

	/**
	 * Create options menu to allow adding of new songs
	 * to playlist.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_add, menu);
		return true;
		
	}

	/**
	 * Callback to handle option clicked
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings_add_menu) {
			// Start Preferences activity
			startActivity(new Intent(this, ActivitySettings.class));
			return true;
		}
		else if (id == R.id.action_add) {
			// Show the file browser in order to allow user to add files
			// to playlist
			Intent intent = new Intent(this, ActivityFileBrowser.class);
			startActivityForResult(intent, REQUEST_CODE_FILE_PICKER);
			return true;
		}
		return super.onOptionsItemSelected(item);
		
	}
	
	/**
	 * Helper method to generate a playlist name when
	 * create a new playlist.<br />
	 * This generation will be in the form of "new_playlistX" where X is
	 * a sequential number generated by this function.
	 *
	 * @return An usable, new playlist name.
	 */
	private String findNewPlaylistName() {
		String newName = NEW_PLAYLIST_NAME;
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
		if (!PlayListManager.isthere(plPath, newName))
			return newName;
		
		Integer i = 1;
		
		while (PlayListManager.isthere(plPath, newName + "_" + i.toString())) {
			i++;
		}
		
		return newName + "_" + i.toString();
	}

	/**
	 * Function called when the user close the FileBrowser.<br />
	 * This means that the user has selected a new song to add, or it clicked
	 * cancel button.
	 * @param data Intent whose extra field contain the path of file to add in
	 * playlist
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == REQUEST_CODE_FILE_PICKER && resultCode == RESULT_OK) {
			/*
			 * The user choice a song to add. The path will be added to
			 * playlist file and the ListView adapter will be updated to
			 * reflect this change.
			 * @see addSongToList
			 */
			String songPath = data.getExtras().getString(ActivityFileBrowser.result);
			if (songPath != null) {
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
				String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
				PlayListManager.addSong(songPath, plPath, playlistName);

				addSongToList(songPath);
			}
		}
	}
	
	/**
	 * Helper function to start an AsyncTask that
	 * will update the whole ListView.
	 * Called just once when the Activity is created,
	 * when adding or removing a song, the update will be
	 * selectively applied, so no AsyncTask is needed
	 */
	private void refreshList() {

		list.setVisibility(View.INVISIBLE);
		progress.setVisibility(View.VISIBLE);
		refreshListTask.execute();
	}
	
	/**
	 * Extract the ID3 metadata information and create
	 * a new item in the ListAdapter. <br />
	 * Then the UI will be notified of the change.
	 * @param path The path of song to add
	 */
	private void addSongToList(String path) {
		songsPath.add(path);
		
		String title = Id3Tag.getTitle(path);
		String artist = Id3Tag.getArtist(path);
		String length = Id3Tag.getLength(path);

		HashMap<String, String> song = new HashMap<String, String>();
		song.put(SongAdapter.KEY_TITLE, title != null ? title : Uri.parse(path).getLastPathSegment());
		song.put(SongAdapter.KEY_ARTIST, artist);
		song.put(SongAdapter.KEY_DURATION, length);
		song.put(SongAdapter.KEY_PLAYING, "");
		//song.put(SongAdapter.KEY_THUMB, Id3Tag.getAlbumPath(ActivityManageSongsInPlaylist.this, path));
		
		playlist.add(song);

		listAdapter.notifyDataSetChanged();
	}
	
	/**
	 * Remove an item from the playlist.<br />
	 * The ListAdapter will be updated accordingly and
	 * the UI notified about the change.
	 * @param index The index of item to remove
	 */
	private void removeSongFromList(int index) {

		songsPath.remove(index);
		playlist.remove(index);

		listAdapter.notifyDataSetChanged();
	}

	/**
	 * AsyncTask extension to load paths in the ListView.
	 * After the task ends, another AsyncTask will be called to
	 * fill in ID3 Tag details.
	 * @see refreshLazyList
	 */
	private class refreshRawList extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ActivityManageSongsInPlaylist.this);
			String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
			
			songsPath = PlayListManager.getSongsString(plPath, playlistName);
			playlist.clear();
			for (int i = 0; i < songsPath.size(); i++) {
				String songPath = songsPath.get(i);
				HashMap<String, String> song = new HashMap<String, String>();
				song.put(SongAdapter.KEY_TITLE, songPath.substring(songPath.lastIndexOf("/")+1));
				
				playlist.add(song);
			}
			
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			listAdapter.notifyDataSetChanged();
			
			list.setVisibility(View.VISIBLE);
			progress.setVisibility(View.INVISIBLE);

			/*
			 * Launch another asynctask to populate ID3 Tags
			 * @see refreshLazyList
			 */
			refreshListTask = new refreshLazyList();

			refreshListTask.execute();
		}
	}

	/**
	 * AsyncTask extension to load ID3 Tags in the ListView.
	 */
	private class refreshLazyList extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ActivityManageSongsInPlaylist.this);
			String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
			
			songsPath = PlayListManager.getSongsString(plPath, playlistName);
			for (int i = 0; i < songsPath.size(); i++) {
				String songPath = songsPath.get(i);
				HashMap<String, String> song = playlist.get(i);

				String title = Id3Tag.getTitle(songPath);

				song.put(SongAdapter.KEY_TITLE, title != null ? title : Uri.parse(songPath).getLastPathSegment());
				song.put(SongAdapter.KEY_ARTIST, Id3Tag.getArtist(songPath));
				song.put(SongAdapter.KEY_DURATION, Id3Tag.getLength(songPath));
				//song.put(SongAdapter.KEY_THUMB, Id3Tag.getAlbumPath(ActivityManageSongsInPlaylist.this, songPath));
				
				publishProgress();
			}
			
			
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
			
			listAdapter.notifyDataSetChanged();
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			listAdapter.notifyDataSetChanged();
		}
	}
	
	/**
	 * Called when the activity is about to be destroyed.
	 * Acts like a destructor.<br />
	 * All resources will be freed (MediaPlayer) and any pending
	 * task will be cancelled.
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		refreshListTask.cancel(true);

		if (previewHandle != null) {
			previewHandle.cancel(true);
		}
		
		player.reset();
		player.release();
		player = null;

		Log.i(TAG, "Destroyed");
	}

	/**
	 * Asyncrhonously called by the MediaPlayer when is ready to reproduce
	 * song.<br />
	 * This method will take preview time from the setting and start playback
	 * seeking about at 1/3 of song, scheduling a runnable that defer the stop
	 * at <code>t = current_time + preview_time.</code><br />
	 * This way the preview will be stopped after preview_time seconds.
	 * The UI will be also changed to show a playback symbol near the current song.
	 * @see stopPlayerPreview
	 */
	@Override
	public void onPrepared(MediaPlayer mp) {
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

		String previewTimeStr = pref.getString(getString(R.string.pref_key_player_preview), DEFAULT_PREVIEW_TIME);

		int previewSeconds = Integer.valueOf(previewTimeStr);

		Log.i(TAG, "Started preview for " + previewSeconds + " seconds.");

		int seek = player.getDuration()/3;
		
		player.seekTo(seek);
		player.start();
		
		previewHandle = executor.schedule(stopPlayerPreview, previewSeconds, TimeUnit.SECONDS);

		// Put playback symbol in the list item
		HashMap<String, String> song = playlist.get(previewIndex);

		song.put(SongAdapter.KEY_PLAYING, SongAdapter.IS_PLAYING);

		listAdapter.notifyDataSetChanged();
	}
}
