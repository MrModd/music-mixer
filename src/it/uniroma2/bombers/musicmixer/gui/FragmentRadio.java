package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.MainActivity;
import it.uniroma2.bombers.musicmixer.PlaybackService;
import it.uniroma2.bombers.musicmixer.R;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

public class FragmentRadio extends Fragment implements PlaybackService.OnErrorListener {
	public static final String PREF_KEY_RADIO_SELECTED = "radio selected";
	public static final String PREF_KEY_RADIO_URL = "radio url";
	
	private PlaybackService mService = null;
	private String streamUrl = null;
	
	private ImageButton stopButton;
	private ImageButton startButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//This method is called when the View must draw this fragment for the first time
		//The inflater insert this fragment in the fragment container
		
		View v = inflater.inflate(R.layout.fragment_radio, container, false);
		
		stopButton = (ImageButton)v.findViewById(R.id.radioButtonStop);
		startButton = (ImageButton)v.findViewById(R.id.radioButtonPlay);
		
		//Listeners
		stopButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mService != null) {
					mService.stop();
					
					startButton.setEnabled(true);
					stopButton.setEnabled(false);
				}
			}
			
		});
		
		startButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mService != null) {
					ArrayList<String> url = new ArrayList<String>();
					url.add(streamUrl);
					mService.setPlaylist(url);
					mService.play();
					
					startButton.setEnabled(false);
					stopButton.setEnabled(true);

					//Save state: FragmentRadio was last view selected by user
					SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
					pref.edit()
						.putBoolean(FragmentPlayer.PREF_KEY_PLAYER_SELECTED, false)
						.putBoolean(PREF_KEY_RADIO_SELECTED, true)
						.commit();

					//Change sharing message
					MainActivity ma = (MainActivity)getActivity();
					ma.setSong(getResources().getString(R.string.action_share_song_message_default));
				}
			}
			
		});
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		//Restore state
		if (mService != null) {
			
			startButton.setEnabled(true);
			stopButton.setEnabled(false);
			
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
			if (pref.getBoolean(PREF_KEY_RADIO_SELECTED, false)) {
				String pl = pref.getString(PREF_KEY_RADIO_URL, "");
				if (pl.length() > 0) {
					streamUrl = pl;
				}
				if (mService.isPlaying()) {
					startButton.setEnabled(false);
					stopButton.setEnabled(true);
				}
			}
			
			mService.setOnErrorListener(this);
		}
		else {
			
			startButton.setEnabled(false);
			stopButton.setEnabled(false);
			
			Toast.makeText(getActivity(), getResources().getString(R.string.fragment_radio_toast_error), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		//Save state
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		pref.edit()
			.putString(PREF_KEY_RADIO_URL, streamUrl)
			.commit();
		
		if (mService != null) {
			mService.setOnErrorListener(null);
		}
	}

	@Override
	public void onError(int what, int extra) {
		startButton.setEnabled(false);
		stopButton.setEnabled(false);

		if (isAdded()) {
			//Save state
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
			pref.edit()
				.putBoolean(PREF_KEY_RADIO_SELECTED, false)
				.commit();
		
			Toast.makeText(getActivity(), R.string.fragment_radio_toast_url_invalid, Toast.LENGTH_LONG).show();
		}
	}
	
	/**
	 * 
	 * @param service the PlaybackService currently binded to the Activity or null
	 */
	public void setService(PlaybackService service) {
		mService = service;
		
		if (service != null) {
			Log.i("FragmentRadio", "Setting service");
			mService.setOnErrorListener(this);
		}
		else
			Log.i("FragmentRadio", "Unsetting service: instance was null");
	}
	
	/**
	 * Selecte the source for the radio stream
	 * @param url the URL for the web audio stream
	 */
	public void setUrl(String url) {
		streamUrl = url;
		Log.i("FragmentRadio", "Set url to: " + streamUrl);
	}

}
