package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.Id3Tag;
import it.uniroma2.bombers.musicmixer.R;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class FragmentSettings extends PreferenceFragment {
	public static final String DEF_PATH_PLAYLISTS = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Playlists/";
	public static final String DEF_PATH_REC = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Podcasts/";
	public static final Integer PLAYLIST_FOLDER_PICKER_TAG = 87;
	public static final Integer RECORDINGS_FOLDER_PICKER_TAG = 50;
	
	private Preference playlistsFolder;
	private Preference recordingsFolder;
	private Preference historyClear;
	private Preference cacheClear;
	private Preference graphPref;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.settings);
		
		playlistsFolder = (Preference)findPreference(getString(R.string.pref_key_player_playlists_path));
		recordingsFolder = (Preference)findPreference(getString(R.string.pref_key_recorder_folder));
		historyClear = (Preference)findPreference(getString(R.string.pref_key_radio_history_clear));
		cacheClear = (Preference)findPreference(getString(R.string.pref_key_player_clear_cache));
		graphPref = (Preference)findPreference(getString(R.string.pref_key_player_analyzer));
		
		//Retrieve selected path or set default path for recordings
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		playlistsFolder.setSummary(pref.getString(getString(R.string.pref_key_player_playlists_path), DEF_PATH_PLAYLISTS));
		recordingsFolder.setSummary(pref.getString(getString(R.string.pref_key_recorder_folder), DEF_PATH_REC));
		cacheClear.setSummary(getDimension(getActivity().getCacheDir() + Id3Tag.THUMBNAIL_FOLDER));
		historyClear.setSummary(getDimension(getActivity().getCacheDir() + "/" + DialogChooseRadioStream.HISTORY_FILE));
		
		
		
		//Listeners
		playlistsFolder.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				
				startActivityForResult(new Intent(getActivity(), ActivityDirectoryBrowser.class), PLAYLIST_FOLDER_PICKER_TAG);
				
				return true;
				
			}
			
		});
		
		recordingsFolder.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				
				startActivityForResult(new Intent(getActivity(), ActivityDirectoryBrowser.class), RECORDINGS_FOLDER_PICKER_TAG);
				return true;
			}
			
		});
		
		historyClear.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				File f = new File(getActivity().getCacheDir(), DialogChooseRadioStream.HISTORY_FILE);
				f.delete();
				
				Toast.makeText(getActivity(), R.string.pref_radio_history_cleared, Toast.LENGTH_SHORT).show();
				return true;
			}
		});
		
		historyClear.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				File f = new File(getActivity().getCacheDir(), DialogChooseRadioStream.HISTORY_FILE);
				f.delete();

				historyClear.setSummary(getDimension(getActivity().getCacheDir() + "/" + DialogChooseRadioStream.HISTORY_FILE));
				Toast.makeText(getActivity(), R.string.pref_radio_history_cleared, Toast.LENGTH_SHORT).show();
				return true;
			}
		});
		
		cacheClear.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				File f = new File(getActivity().getCacheDir()+Id3Tag.THUMBNAIL_FOLDER);
				deleteDirectory(f);

				cacheClear.setSummary(getDimension(getActivity().getCacheDir() + Id3Tag.THUMBNAIL_FOLDER));
				Toast.makeText(getActivity(), R.string.pref_player_cache_cleared, Toast.LENGTH_SHORT).show();
				return true;
			}
		});
		
		graphPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Toast.makeText(getActivity(), R.string.pref_player_spectrum_change, Toast.LENGTH_SHORT).show();
				return true;
			}
		});

		bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_player_loop_number)));
		bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_player_fade_level)));
		bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_player_preview)));
		bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_radio_history_length)));
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == PLAYLIST_FOLDER_PICKER_TAG && resultCode == Activity.RESULT_OK) {
			
			String chosenDir = data.getExtras().getString(ActivityDirectoryBrowser.result);

			chosenDir = chosenDir + "/";
			if (chosenDir.equals("//")) {
				chosenDir = "/";
			}
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
			Editor editor = pref.edit();
			editor.putString(getString(R.string.pref_key_player_playlists_path), chosenDir);
			editor.commit();
			
			playlistsFolder.setSummary(chosenDir);
		}
		
		else if (requestCode == RECORDINGS_FOLDER_PICKER_TAG && resultCode == Activity.RESULT_OK) {
			
			String chosenDir = data.getExtras().getString(ActivityDirectoryBrowser.result);

			chosenDir = chosenDir + "/";
			if (chosenDir.equals("//")) {
				chosenDir = "/";
			}
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
			Editor editor = pref.edit();
			editor.putString(getString(R.string.pref_key_recorder_folder), chosenDir);
			editor.commit();
			
			recordingsFolder.setSummary(chosenDir);
		}
	}
	
	
	
	/**
	 * A preference value change listener that updates the preference's summary
	 * to reflect its new value.
	 */
	private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object value) {
			String stringValue = value.toString();

			if (preference instanceof ListPreference) {
				// For list preferences, look up the correct display value in
				// the preference's 'entries' list.
				ListPreference listPreference = (ListPreference) preference;
				int index = listPreference.findIndexOfValue(stringValue);

				// Set the summary to reflect the new value.
				preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);

			} else {
				// For all other preferences, set the summary to the value's
				// simple string representation.
				preference.setSummary(stringValue);
			}
			return true;
		}
	};

	/**
	 * Binds a preference's summary to its value. More specifically, when the
	 * preference's value is changed, its summary (line of text below the
	 * preference title) is updated to reflect the value. The summary is also
	 * immediately updated upon calling this method. The exact display format is
	 * dependent on the type of preference.
	 * 
	 * @see #sBindPreferenceSummaryToValueListener
	 */
	private static void bindPreferenceSummaryToValue(Preference preference) {
		// Set the listener to watch for value changes.
		preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		// Trigger the listener immediately with the preference's
		// current value.
		sBindPreferenceSummaryToValueListener.onPreferenceChange(
				preference,
				PreferenceManager.getDefaultSharedPreferences(
						preference.getContext()).getString(preference.getKey(),
						""));
	}
	
	private String getDimension(String path) {
		File f = new File(path);
		long s = pathSize(f);
		String unit = " B";
		
		if (s / 1024 > 0) {
			unit = " KB";
			s /= 1024;
			
			if (s / 1024 > 0) {
				unit = " MB";
				s /= 1024;
				
				if (s / 1024 > 0) {
					unit = " GB";
					s /= 1024;
				}
			}
		}
		return new String(s + unit);
	}
	
	/**
	 * Obtain the dimension of a directory and all included files
	 * @param f File descriptor of a directory
	 * @return recursive dimension in bytes of all files contained in f
	 */
	public static long pathSize(File f) {
		if (f == null || !f.exists())
			return 0;
		if (f.isFile())
			return f.length();
	    long length = 0;
	    for (File file : f.listFiles()) {
	        if (file.isFile())
	            length += file.length();
	        else
	            length += pathSize(file);
	    }
	    return length;
	}
	
	/**
	 * Delete recursively a directory
	 * @param file File descriptor of a directory
	 */
	public static void deleteDirectory(File file) {
		if(file.isDirectory()){
			//directory is empty, then delete it
			if(file.list().length == 0){
				file.delete();
			} else {
				//list all the directory contents
				String files[] = file.list();
 
				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);
 
					//recursive delete
					deleteDirectory(fileDelete);
				}
				//check the directory again, if empty then delete it
				if(file.list().length == 0){
					file.delete();
				}
			}
		} else {
			//if file, then delete it
			file.delete();
		}
	} 

}
