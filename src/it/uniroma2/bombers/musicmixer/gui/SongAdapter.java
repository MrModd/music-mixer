package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.Id3Tag;
import it.uniroma2.bombers.musicmixer.R;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This class implements an adapter for a list view that prints a thumbnail of the album,
 * the title of the song, its author and the duration.
 * 
 * @author Cosentino, De Angelis, Picierro, Di Francescangelo
 *
 */
public class SongAdapter extends BaseAdapter {
	public static final String KEY_TITLE = "key_title";
	public static final String KEY_ARTIST = "key_artist";
	public static final String KEY_DURATION = "key_duration";
	public static final String KEY_THUMB = "key_thumb";
	public static final String KEY_PLAYING = "key_playing";
	public static final String IS_PLAYING = "is_playing";
	public static final String IS_PAUSED = "is_paused";
	
	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private static LayoutInflater inflater = null;
	
	/**
	 * Create a new adapter that should be used in a ListView
	 * 
	 * @param a The parent activity
	 * @param d A list of hashmaps: every map should represent an element of the ListView.
	 *          The elements of every map should have IDs from the constants pool of this class.
	 */
	public SongAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
		activity = a;
		data = d;
		if (activity != null) {
			inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
	}

	@Override
	public int getCount() {
		if (data != null)
			return data.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (data != null)
			return data.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (convertView == null) {
			if (inflater == null)
				return null;
			v = inflater.inflate(R.layout.song_adapter, null);
		}
		
		TextView title = (TextView)v.findViewById(R.id.song_adapter_title);
		TextView artist = (TextView)v.findViewById(R.id.song_adapter_artist);
		TextView duration = (TextView)v.findViewById(R.id.song_adapter_duration);
		ImageView thumb = (ImageView)v.findViewById(R.id.song_adapter_thumbnail);
		ImageView play = (ImageView)v.findViewById(R.id.song_adapter_play_icon);
		ImageView pause = (ImageView)v.findViewById(R.id.song_adapter_pause_icon);

		String current_title = null;
		String current_artist = null;
		String current_duration = null;
		String current_play_state = null;
		String current_thumb_path = null;

		int play_visibility = View.INVISIBLE;
		int pause_visibility = View.INVISIBLE;

		// 1) TRY LOAD DATA FROM CURRENT ADAPTER
		if (data != null) {
			HashMap<String, String> song = data.get(position);

			current_title = song.get(KEY_TITLE);
			current_artist = song.get(KEY_ARTIST);
			current_duration = song.get(KEY_DURATION);
			current_play_state = song.get(KEY_PLAYING);
			current_thumb_path = song.get(KEY_THUMB);
		}

		// 2) PERFORM SOME DATA SANITY CHECKS
		if (current_title == null)
			current_title = activity.getString(R.string.song_adapter_def_title);

		if (current_artist == null)
			current_artist = activity.getString(R.string.song_adapter_def_artist);

		if (current_duration == null)
			current_duration = activity.getString(R.string.song_adapter_def_duration);

		if (current_play_state != null) {
			if (current_play_state == IS_PLAYING) {
				play_visibility = View.VISIBLE;
			}
			else if (current_play_state == IS_PAUSED) {
				pause_visibility = View.VISIBLE;
			}
		}

		// 3) COMMIT CHANGES TO VIEW
		title.setText(current_title);
		artist.setText(current_artist);
		duration.setText(current_duration);

		play.setVisibility(play_visibility);
		pause.setVisibility(pause_visibility);

		if (current_thumb_path != null) {
			thumb.setImageBitmap(Id3Tag.getThumbnailBitmap(current_thumb_path, 50, 50));
		}
		else {
			thumb.setImageResource(R.drawable.def_album_thumb);
		}

		return v;
	}

}
