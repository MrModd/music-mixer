package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class DialogRename extends DialogFragment {
	
	private String previousText = null;
	private TextView inputText;
	
    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;
	
	/* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String newName);
        public void onDialogNegativeClick(DialogFragment dialog);
    }
	
	public void setOnDialogClickListener(NoticeDialogListener listener) {
		mListener = listener;
	}

	public void setPreviousText(String name) {
		previousText = name;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();
		
		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		View v = inflater.inflate(R.layout.dialog_rename, null);
		builder.setView(v);
		
		inputText = (TextView)v.findViewById(R.id.dialogRenameEditText);
		if (previousText != null) {
			inputText.setText(previousText);
		}
		
		//Add elements in the dialog box
		builder.setTitle(R.string.dialog_rename_title);
		
		builder.setPositiveButton(R.string.dialog_rename_ok_button, new OnClickListener () {
		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mListener != null) {
					mListener.onDialogPositiveClick(DialogRename.this, inputText.getText().toString());
				}
			}
			
		});
		builder.setNegativeButton(R.string.dialog_radio_Stream_cancel_button, new OnClickListener () {
		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mListener != null) {
					mListener.onDialogNegativeClick(DialogRename.this);
				}
			}
			
		});
		
		return builder.create();
	}

}
