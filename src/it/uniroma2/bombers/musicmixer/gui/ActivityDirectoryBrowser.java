package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ActivityDirectoryBrowser extends Activity {
	
	public static final String result = "result";
	public static final String exceptionText = "Permission denied";
	public static final String CURRENT_DIR_FILE_PICKER_PREF = "Current directory";
	
	private ArrayList<File> directoryListing;//Directory listing
	FileAdapter fa; 						//List view adapter
	String actualPath;				//Actual path
	String[] formats = {"mp3", "3gp", "wma", "flac", "3gpp"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_directory_browser);
		
		
		actualPath = PreferenceManager.getDefaultSharedPreferences(this).getString(CURRENT_DIR_FILE_PICKER_PREF, "/");
		
		ListView lv = (ListView)findViewById(R.id.directory_browser_content_list);
		directoryListing = new ArrayList<File>();
		File f = new File(actualPath);
		
		if(!f.exists()){
			actualPath="/";
			f = new File(actualPath);
			PreferenceManager.getDefaultSharedPreferences(ActivityDirectoryBrowser.this).edit().putString(CURRENT_DIR_FILE_PICKER_PREF, actualPath).commit();
		}
		
		//Listing of the root directory
		try {
			directoryListing = listing(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		fa = new FileAdapter(this, directoryListing);
		lv.setAdapter(fa);
		
		//Listener on click to a file into the directory
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				//The selection is a directory -> listing
				if(directoryListing.get(arg2).isDirectory()){
					ArrayList<File> temp = null;
					try {
						actualPath = directoryListing.get(arg2).getCanonicalPath();
						temp = listing(directoryListing.get(arg2));
					} catch (IOException e) {
						return;
					}
					directoryListing.clear();
					directoryListing.addAll(temp);
					fa.notifyDataSetChanged();
				}
			}
			
		});
	}
	
	
	private ArrayList<File> listing(File f) throws IOException{
		ArrayList<File> collection = new ArrayList<File>();
		
		//If the directory isn't the root directory, add to the listing the .. directory
		if(f.getCanonicalPath().length()>1){
			File previosDir = new File(f.getCanonicalPath()+"/..");
			collection.add(previosDir);
		}
		
		File[] listing = f.listFiles();
		//If listing == null, permission denied
		if(listing==null){
			CharSequence text = getResources().getString(R.string.error_explorer);
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
			throw new IOException(exceptionText);     
		}
		
		for(int i = 0; i<listing.length; i++){
			File tmp = listing[i];
			listing[i]=tmp.getCanonicalFile();
		}
		
		Arrays.sort(listing);
		collection.addAll(Arrays.asList(listing));
		//Filtering all the not-audio files
		filter(collection);
		
		return collection;
	}
	
	private void filter(ArrayList<File> list){
		for(int i=0;i<list.size();i++){
			File tmp = list.get(i);
			//Directories are allowed. Else continue
			if(!tmp.isDirectory()){
				//Getting all the files extensions
				String[] name = tmp.getName().split("\\.");
				//If no extension, remove
				if(name.length<2 || tmp.isHidden()){
					list.remove(i);
					i--;
				}
				else{
					//If supported format, keep the file. Otherwise remove 
					boolean ctrl = false;
					for(int j=0; j<formats.length; j++){
						if(name[name.length-1].equals(formats[j])){
							ctrl=true;
						}
					}
					if(!ctrl){
						list.remove(i);
						i--;
					}
				}
			}
		}
	}
	
	/**
	 * Function called pressing the ok button
	 * @param v
	 * @throws IOException
	 */
	public void okFunction(View v) throws IOException{
		Intent returnIntent = new Intent();
		if(actualPath.equals("")){
			actualPath="/";
		}
		returnIntent.putExtra(result,actualPath);
		
		PreferenceManager.getDefaultSharedPreferences(ActivityDirectoryBrowser.this).edit().putString(CURRENT_DIR_FILE_PICKER_PREF, actualPath).commit();
		
		setResult(RESULT_OK,returnIntent);
		finish();
	}
	
	/**
	 * Function called pressing the cancel button
	 * @param v
	 * @throws IOException
	 */
	public void cancelFunction(View v) throws IOException{
		Intent returnIntent = new Intent();
		setResult(RESULT_CANCELED,returnIntent);
		finish();
	}
	
	//Override to permit the navigation of the parent directory on the onBack pressing
	@Override
	public void onBackPressed() {
	   if(actualPath.length()<=1){
		   super.onBackPressed();
		   return;
	   }
	   ArrayList<File> temp = null;
	   File f = null;
		try {
			f = (new File(actualPath+"/..")).getAbsoluteFile();
			actualPath = f.getCanonicalPath();
			temp = listing(f);
		} catch (IOException e) {
			return;
		}
		directoryListing.clear();
		directoryListing.addAll(temp);
		fa.notifyDataSetChanged();
	}

}