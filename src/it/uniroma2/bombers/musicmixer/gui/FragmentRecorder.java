package it.uniroma2.bombers.musicmixer.gui;

import java.io.IOException;

import it.uniroma2.bombers.musicmixer.RecorderService;
import it.uniroma2.bombers.musicmixer.R;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

public class FragmentRecorder extends Fragment {
	
	public static final String logTag = "FragmRecorder";
	
	RecorderService rec = null;
	ImageButton startBtn;
	ImageButton stopBtn;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//This method is called when the View must draw this fragment for the first time
		//The inflater insert this fragment in the fragment container
		
		View v = inflater.inflate(R.layout.fragment_recorder, container, false);
		startBtn = (ImageButton) v.findViewById(R.id.recorderButtonRecord);
		stopBtn = (ImageButton) v.findViewById(R.id.recorderButtonStop);
			
		startBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					rec.startRec(getActivity());
					startBtn.setEnabled(false);
					stopBtn.setEnabled(true);
				} catch (IOException e) {
					Log.e(logTag, "Error starting recorder service");
					Toast.makeText(getActivity(), getString(R.string.fragment_recorder_cannot_record), Toast.LENGTH_LONG).show();
					startBtn.setEnabled(false);
					stopBtn.setEnabled(false);
				}
			}
		});
		
		stopBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startBtn.setEnabled(true);
				stopBtn.setEnabled(false);
				rec.stopRec();
			}
		});
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(rec == null){
			CharSequence text = getResources().getString(R.string.record_not_started);
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(getActivity(), text, duration);
            toast.show();
            startBtn.setEnabled(false);
            stopBtn.setEnabled(false);
		}
		else if(rec.status()){
			startBtn.setEnabled(false);
			stopBtn.setEnabled(true);
		}
		else{
			startBtn.setEnabled(true);
			stopBtn.setEnabled(false);
		}
	}
	
	/**
	 * Set the RecorderService to the fragment
	 * @param recorder
	 */
	public void setRecorder(RecorderService recorder){
		rec = recorder;
	}
}
