package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.PlayListManager;
import it.uniroma2.bombers.musicmixer.R;
import it.uniroma2.bombers.musicmixer.gui.DialogPlaylistOptionsMenu.DialogPlaylistOptionsMenuListener;
import it.uniroma2.bombers.musicmixer.gui.DialogRename.NoticeDialogListener;

import java.util.ArrayList;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ActivityManagePlaylists extends Activity {
	
	public static final String DIALOG_PLAYLIST_OPTION_TAG = "playlist options";
	public static final String DIALOG_RENAME_TAG = "dialog rename";
	
	private ListView list;
	private ArrayAdapter<String> listAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_list_view);
		
		list = (ListView)findViewById(R.id.activityListView);
		
		//Use a default layout for this ListView
		listAdapter = new ArrayAdapter<String>(
				this,
				android.R.layout.simple_list_item_2,
				android.R.id.text1);
		
		list.setAdapter(listAdapter);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		refreshList();
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				String plName = listAdapter.getItem(position);
				
				//Open ActivityManageSongsInPlaylist for current playlist
				Intent intent = new Intent(ActivityManagePlaylists.this, ActivityManageSongsInPlaylist.class);
				intent.putExtra(ActivityManageSongsInPlaylist.EXTRA_NEW_PLAYLIST, false);
				intent.putExtra(ActivityManageSongsInPlaylist.EXTRA_PLAYLIST_NAME, plName);
				startActivity(intent);
			}
			
		});
		
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				final String plName = listAdapter.getItem(position);
				
				//Show dialog option menu
				DialogPlaylistOptionsMenu dialog = new DialogPlaylistOptionsMenu();
				dialog.setPlaylistName(plName);
				dialog.setOnRequestDialogListener(new DialogPlaylistOptionsMenuListener() {

					@Override
					public void onRequestDialogDelete(DialogFragment dialog) {
						
						SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ActivityManagePlaylists.this);
						String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
						PlayListManager.removePlaylist(plPath, plName);
						ActivityManagePlaylists.this.refreshList();
						
					}

					@Override
					public void onRequestDialogRename(DialogFragment dialog) {
						DialogRename dialogRename = new DialogRename();
						dialogRename.setPreviousText(plName);
						dialogRename.show(getFragmentManager(), DIALOG_RENAME_TAG);
						dialogRename.setOnDialogClickListener(new NoticeDialogListener() {

							@Override
							public void onDialogPositiveClick(
									DialogFragment dialog, String newName) {
								
								SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ActivityManagePlaylists.this);
								
								String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
								
								PlayListManager.renamePlaylist(plPath, plName, newName);
								ActivityManagePlaylists.this.refreshList();
								
							}

							@Override
							public void onDialogNegativeClick(DialogFragment dialog) {
							}
							
						});
					}

					@Override
					public void onRequestDialogModify(DialogFragment dialog) {
						Intent intent = new Intent(ActivityManagePlaylists.this, ActivityManageSongsInPlaylist.class);
						intent.putExtra(ActivityManageSongsInPlaylist.EXTRA_NEW_PLAYLIST, false);
						intent.putExtra(ActivityManageSongsInPlaylist.EXTRA_PLAYLIST_NAME, plName);
						startActivity(intent);
					}
					
				});
				dialog.show(getFragmentManager(), DIALOG_PLAYLIST_OPTION_TAG);
				
				return true;
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_add, menu);
		return true;
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		int id = item.getItemId();
		
		if (id == R.id.action_settings_add_menu) {
			// Start Preferences activity
			startActivity(new Intent(this, ActivitySettings.class));
			return true;
		}
		else if (id == R.id.action_add) {
			//Add a playlist
			Intent intent = new Intent(this, ActivityManageSongsInPlaylist.class);
			intent.putExtra(ActivityManageSongsInPlaylist.EXTRA_NEW_PLAYLIST, true);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void refreshList() {
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
		ArrayList<String> playlists = PlayListManager.getPlayList(plPath);
		listAdapter.clear();
		listAdapter.addAll(playlists);
		
		listAdapter.notifyDataSetChanged();
		
	}
}
