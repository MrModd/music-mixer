package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.Id3Tag;
import it.uniroma2.bombers.musicmixer.MainActivity;
import it.uniroma2.bombers.musicmixer.PlayListManager;
import it.uniroma2.bombers.musicmixer.PlaybackService;
import it.uniroma2.bombers.musicmixer.PlaybackService.OnChangeListener;
import it.uniroma2.bombers.musicmixer.R;
import it.uniroma2.bombers.musicmixer.visualizer.LineGraphRenderer;
import it.uniroma2.bombers.musicmixer.visualizer.VisualizerView;

import java.io.File;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class FragmentPlayer extends Fragment implements OnChangeListener {
	public static final String PREF_KEY_PLAYER_SELECTED = "player selected";
	public static final String PREF_KEY_PLAYER_DIRECTORY = "player is directory";
	public static final String PREF_KEY_PLAYER_PLNAME = "player playlist name";
	public static final String PREF_KEY_PLAYER_CURRENT_INDEX = "player current index";
	public static final String PREF_KEY_PLAYER_SHUFFLE = "player shuffle";
	
	private ListView list;
	private ProgressBar progress;
	private SongAdapter listAdapter;
	private ImageView albumArt;
	private VisualizerView graph;
	
	private SeekBar seekBar;
	private ImageButton prevButton;
	private ImageButton stopButton;
	private ImageButton startButton;
	private ImageButton nextButton;
	private ImageButton shuffleButton;
	
	private String playlistName = null;
	private ArrayList<String> songsPath;
	private ArrayList<HashMap<String, String>> playlist;

	private refreshListAsync asyncRefresh = null;
	
	private PlaybackService mService = null;
	private boolean playlistIsSettled = false;
	private Integer currentIndex = -1;
	private boolean shuffle = false;
	
	private boolean firstLoading; //If true force to call UpdateAdapters(-1, currentIndex) after completion of AsyncTask
	
	Thread asyncSeek;
	
	private Boolean isDirectory = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//This method is called when the View must draw this fragment for the first time
		//The inflater insert this fragment in the fragment container
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		
		Log.i("FragmentPlayer", "OnCreateView");
		
		boolean graphOn = pref.getBoolean(getString(R.string.pref_key_player_analyzer), false);
		
		View v = inflater.inflate(R.layout.fragment_player, container, false);
		
		list = (ListView)v.findViewById(R.id.playlistView);
		progress = (ProgressBar)v.findViewById(R.id.progressBarCurrentPlaylist);
		albumArt = (ImageView)v.findViewById(R.id.imageAlbumArt);
		graph = (VisualizerView)v.findViewById(R.id.graph_visualizer);
		
		if(graphOn){
			albumArt.setVisibility(View.INVISIBLE);
			graph.setVisibility(View.VISIBLE);
		}
		
		seekBar = (SeekBar)v.findViewById(R.id.seekBarPlayer);
		prevButton = (ImageButton)v.findViewById(R.id.playerButtonPrev);
		stopButton = (ImageButton)v.findViewById(R.id.playerButtonStop);
		startButton = (ImageButton)v.findViewById(R.id.playerButtonPlayPause);
		nextButton = (ImageButton)v.findViewById(R.id.playerButtonNext);
		shuffleButton = (ImageButton)v.findViewById(R.id.shuffleButton);
		
		setButtonsEnabled(false);
		
		playlist = new ArrayList<HashMap<String, String>>();
		listAdapter = new SongAdapter(getActivity(), playlist);

		list.setAdapter(listAdapter);
		
		
		
		//Listeners
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				if (mService != null) {
					
					//Save state: FragmentPlayer was last view selected by user
					SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
					if (!pref.getBoolean(PREF_KEY_PLAYER_SELECTED, false)) {
						pref.edit()
							.putBoolean(PREF_KEY_PLAYER_SELECTED, true)
							.putBoolean(FragmentRadio.PREF_KEY_RADIO_SELECTED, false)
							.commit();
					}
					
					if (!playlistIsSettled) {
						//Send all configuration to Service before starting playing
						settleService();
						playlistIsSettled = true;
					}
					
					int prevIndex = currentIndex;
					currentIndex = mService.play(position);
					
					//Change UI configuration
					updateAdapters(prevIndex, currentIndex);
					startButton.setImageResource(R.drawable.ic_media_pause);
					setButtonsEnabled(true);
					if (asyncSeek == null) {
						asyncSeek = new Thread(seekRunnable);
						asyncSeek.start();
					}
				}
			}
			
		});
		
		prevButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i("FragmentPlayer", "Prev Button");
				if (mService != null) {
					int prevIndex = currentIndex;
					currentIndex = mService.prev();
					updateAdapters(prevIndex, currentIndex);
					startButton.setImageResource(R.drawable.ic_media_pause);
				}
			}
			
		});
		
		stopButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i("FragmentPlayer", "Stop Button");
				if (mService != null) {
					mService.stop();
					updateAdapters(currentIndex, -1);
					currentIndex = -1;
					startButton.setImageResource(R.drawable.ic_media_play);
					setButtonsEnabled(false);
					asyncSeek.interrupt();
					asyncSeek = null;
				}
			}
			
		});
		
		startButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Log.i("FragmentPlayer", "Start Button");
				if (playlistName != null && mService != null) {
					
					//Save state: FragmentPlayer was last view selected by user
					SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
					if (!pref.getBoolean(PREF_KEY_PLAYER_SELECTED, false)) {
						pref.edit()
							.putBoolean(PREF_KEY_PLAYER_SELECTED, true)
							.putBoolean(FragmentRadio.PREF_KEY_RADIO_SELECTED, false)
							.commit();
					}
					
					if (!playlistIsSettled) {
						//Send all configuration to Service before starting playing
						settleService();
						playlistIsSettled = true;
					}
					
					if (!mService.isPlaying() && !mService.isPaused()) {
						//To start
						currentIndex = mService.play();
						updateAdapters(-1, currentIndex);

						Log.i("FragmentPlayer", "Starting new song: id=" + currentIndex);
						
						startButton.setImageResource(R.drawable.ic_media_pause);
						setButtonsEnabled(true);
						asyncSeek = new Thread(seekRunnable);
						asyncSeek.start();
					}
					else if (mService.isPlaying() && !mService.isPaused()) {
						//To pause
						currentIndex = mService.pause();
						updateAdapters(currentIndex, currentIndex);

						Log.i("FragmentPlayer", "Pausing song: id=" + currentIndex);
						
						startButton.setImageResource(R.drawable.ic_media_play);
					}
					else if (mService.isPaused()) {
						//To resume from pause
						currentIndex = mService.play();
						updateAdapters(-1, currentIndex);

						Log.i("FragmentPlayer", "Resuming song: id=" + currentIndex);
						
						startButton.setImageResource(R.drawable.ic_media_pause);
					}
				}
			}
			
		});
		
		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Log.i("FragmentPlayer", "Next Button");
				
				if (mService != null) {
					int prevIndex = currentIndex;
					currentIndex = mService.next();
					
					Log.i("FragmentPlayer", "Next song: prev_id=" + prevIndex + " current_id=" + currentIndex);
					
					if (currentIndex < songsPath.size()) {
						//Next song
						updateAdapters(prevIndex, currentIndex);
						startButton.setImageResource(R.drawable.ic_media_pause);
					}
					else {
						//End playing
						updateAdapters(prevIndex, -1);
						currentIndex = -1;
						startButton.setImageResource(R.drawable.ic_media_play);
						setButtonsEnabled(false);

						asyncSeek.interrupt();
						asyncSeek = null;
					}
				}
			}
			
		});
		
		shuffleButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Log.i("FragmentPlayer", "Shuffle Button");
				
				if (shuffle) {
					shuffleButton.setImageResource(R.drawable.ic_media_shuffle_disabled);
					shuffle = false;
				}
				else {
					shuffleButton.setImageResource(R.drawable.ic_media_shuffle_enabled);
					shuffle = true;
				}
				if (mService != null)
					mService.setShuffle(shuffle);
				
			}
		});
		
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (fromUser && mService != null) {
					mService.setPosition(progress);
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			
		});
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		Log.i("FragmentPlayer", "Resuming fragment");

		restoreState();
	}

	@Override
	public void onChange(int id) {
		//Implementation of PlaybackService listener
		
		if (id < songsPath.size()) {
			//Next song
			updateAdapters(currentIndex, id);
			currentIndex = id;
			seekBar.setMax(mService.getDuration());
		}
		else {
			//End playing
			updateAdapters(currentIndex, -1);
			currentIndex = -1;
			startButton.setImageResource(R.drawable.ic_media_play);
			setButtonsEnabled(false);

			asyncSeek.interrupt();
			asyncSeek = null;
		}
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		Log.i("FragmentPlayer", "Fragment paused");
		
		if (asyncSeek != null) {
			asyncSeek.interrupt();
			asyncSeek = null;
		}
		
		if (asyncRefresh != null) {
			asyncRefresh.cancel(true);
			listAdapter.notifyDataSetChanged();
		}
		
		//Save state
		if (playlistName == null && mService != null && mService.isPlaying()) {
			Log.w("FragmentPlayer", "Inconsistent state!! Probably radio is playing?");
		}
		if (playlistName != null) {
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
			pref.edit()
				.putBoolean(PREF_KEY_PLAYER_DIRECTORY, isDirectory)
				.putString(PREF_KEY_PLAYER_PLNAME, playlistName)
				.putBoolean(PREF_KEY_PLAYER_SHUFFLE, shuffle)
				.putInt(PREF_KEY_PLAYER_CURRENT_INDEX, currentIndex)
				.commit();
			Log.i("FragmentPlayer", "Saving: playlist name=" + playlistName
					+ ", current index=" + currentIndex
					+ " and more data");
		}
		
		if (mService != null)
			mService.setOnChangeListener(null);
		
		//Restore default sharing message
		if (isAdded()) {
			MainActivity ma = (MainActivity)getActivity();
			ma.setSong(getResources().getString(R.string.action_share_song_message_default));
		}
		
		if(graph.linked){
			graph.disable();
			graph.release();
		}
	}
	
	private void setButtonsEnabled(boolean enable) {
		seekBar.setClickable(enable);
		seekBar.setEnabled(enable);
		prevButton.setEnabled(enable);
		stopButton.setEnabled(enable);
		nextButton.setEnabled(enable);
		seekBar.setProgress(0);
		seekBar.setMax(0);
	}
	
	private void settleService() {
		if (mService != null) {
			mService.setPlaylist(songsPath);
			
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
			if (pref.getBoolean(getString(R.string.pref_key_player_fade_enable), false)) {
				String fade = pref.getString(getString(R.string.pref_key_player_fade_level), getString(R.string.pref_player_fade_levels_default));
				mService.setFade(Integer.valueOf(fade));
			}
			else {
				mService.setFade(0);
			}
			String loops = pref.getString(getString(R.string.pref_key_player_loop_number), getString(R.string.pref_player_loop_numbers_default));
			int l = Integer.valueOf(loops);
			Log.i("FragmentPlayer", "Number of loops: " + l);
			if (l >= 0)
				mService.setPlaylistLoops(Integer.valueOf(loops));
			else
				mService.setPlaylistLoops(Integer.MAX_VALUE);
			
			mService.setOnChangeListener(this);
		}
	}
	
	private static String formatQuery(String url, String song, String author) {
		String query = url;
		
		song = Normalizer.normalize(song, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		author = Normalizer.normalize(author, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		
		query += author;
		query += " " + song;
		
		query = query.replace(" ", "+");
		
		while (query.contains("++"))
			query = query.replace("++", "+");
		
		query = query.replace("\"", "");
		
		if (query.length() > 0 && query.charAt(query.length() - 1) == '+')
			query = query.substring(0, query.length() - 1);
		
		return query;
	}
	
	/**
	 * @param prev previous song playing
	 * @param current current song playing
	 */
	private void updateAdapters(int prev, int current) {
		
		Log.i("FragmentPlayer", "Updating adapter from id=" + prev + " to id=" + current);
		
		if (mService != null) {
			
			if (mService.isPlaying() && !mService.isPaused() && current >= 0) {
				//Playing new song
				
				//Change sharing message
				if (isAdded()) {
					MainActivity ma = (MainActivity)getActivity();
					
					String messageToSend;
					String songName = playlist.get(current).get(SongAdapter.KEY_TITLE);
					String songAuthor = playlist.get(current).get(SongAdapter.KEY_ARTIST);
					
					if(songName ==  null) {
						File song = new File(songsPath.get(current));
						songName = song.getName().substring(0,song.getName().lastIndexOf("."));
					}
					if (songAuthor == null) {
						songAuthor = "";
					}
					
					messageToSend = getResources().getString(R.string.action_share_song_message_first)
							+ " \"" + songName
							+ "\" " + getResources().getString(R.string.action_share_song_message_last)
							+ " ";
					messageToSend += formatQuery(
							getResources().getString(R.string.action_share_youtube_query),
							songName,
							songAuthor);
					
					ma.setSong(messageToSend);
				}
				
				//Reset adapter for previous song playing
				if (prev >= 0) {
					HashMap<String, String> song = playlist.get(prev);
					song.put(SongAdapter.KEY_PLAYING, "");
					
					listAdapter.notifyDataSetChanged();
				}
				
				//Set adapter for current song playing
				HashMap<String, String> song = playlist.get(current);
				song.put(SongAdapter.KEY_PLAYING, SongAdapter.IS_PLAYING);
				
				listAdapter.notifyDataSetChanged();
				
				//Change album art
				String albumPath = Id3Tag.getAlbumPath(getActivity(), songsPath.get(current));
				if (albumPath != null) {
					Options opt = new Options();
					Bitmap bm = BitmapFactory.decodeFile(albumPath, opt);
					if (bm != null || opt.outHeight > 0) {
						albumArt.setImageBitmap(bm);
					}
					else {
						albumArt.setImageResource(R.drawable.def_album_cover);
					}
				}
				else {
					albumArt.setImageResource(R.drawable.def_album_cover);
				}

				//Focus on current song
				list.setSelection(current);
				
			}
			else if (mService.isPaused()) {
				//Entering pause state
				
				//Update adapter for current song
				HashMap<String, String> song = playlist.get(current);
				song.put(SongAdapter.KEY_PLAYING, SongAdapter.IS_PAUSED);
				
				listAdapter.notifyDataSetChanged();
				
			}
			else if (prev >= 0){
				//Entering stop state
				
				//Restore default sharing message
				if (isAdded()) {
					MainActivity ma = (MainActivity)getActivity();
					ma.setSong(getResources().getString(R.string.action_share_song_message_default));
				}
				
				//Update adapter for last song played
				HashMap<String, String> song = playlist.get(prev);
				song.put(SongAdapter.KEY_PLAYING, "");
				
				listAdapter.notifyDataSetChanged();
				
				//Restore default album art
				albumArt.setImageResource(R.drawable.def_album_cover);
				
				//Focus list on first element
				list.setSelection(0);
				
			}
		}
		
	}
	
	private void restoreState() {

		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		
		if (mService != null && pref.getBoolean(PREF_KEY_PLAYER_SELECTED, false) && isAdded()) {
			//Do restore only if this Fragment is currently attached to MainActivity and it was last Fragment selected
			
			if (asyncRefresh != null) {
				asyncRefresh.cancel(true);
				listAdapter.notifyDataSetChanged();
			}
			
			Log.i("FragmentPlayer", "Restoring state");
			
			String pl = pref.getString(PREF_KEY_PLAYER_PLNAME, "");
			
			Log.i("FragmentPlayer", "Preference Playlist name: \"" + pl + "\"");
			
			if (pl.length() > 0) {
				
				//There was a playlist, restore variables status
				playlistName = pl;
				isDirectory = pref.getBoolean(PREF_KEY_PLAYER_DIRECTORY, false);
				shuffle = pref.getBoolean(PREF_KEY_PLAYER_SHUFFLE, false);
				currentIndex = pref.getInt(PREF_KEY_PLAYER_CURRENT_INDEX, -1);

				Log.i("FragmentPlayer", "Current playlist: \"" + playlistName + "\"");
				Log.i("FragmentPlayer", "Current index: " + currentIndex);
				
			}

			if (mService.isPlaying() || mService.isPaused()) {
				Log.i("FragmentPlayer", "Service is playing");
				
				//mService is playing, so it must be settled previously
				playlistIsSettled = true;

				//Update UI configuration
				if (mService.isPaused()) {
					startButton.setImageResource(R.drawable.ic_media_play);
				}
				else {
					startButton.setImageResource(R.drawable.ic_media_pause);
				}
				setButtonsEnabled(true);
				asyncSeek = new Thread(seekRunnable);
				asyncSeek.start();
				
				if (shuffle) {
					shuffleButton.setImageResource(R.drawable.ic_media_shuffle_enabled);
				}
				
				if (mService.getCurrentIndex() >= 0) {
					currentIndex = mService.getCurrentIndex();
					Log.i("FragmentPlayer", "Updated current index: " + currentIndex);
				}
				
				//Reattach Service listener
				mService.setOnChangeListener(this);
			}
			
			//It is required an initial call of UpdateAdapters()
			firstLoading = true;
		}
		else {
			Log.i("FragmentPlayer", "Restoring state: can't find service or player fragment wasn't last visited");
			firstLoading = false;
			playlistIsSettled = false;
		}
		
		//Draw visualizer and set settings
		if (mService != null && pref.getBoolean(getString(R.string.pref_key_player_analyzer), false)) {
			graph.linkToPlayer(mService.getAudioSessionId());
			addLineRenderer();
		}

		if (playlistName != null) {
			refreshList();
		}
		
	}
	
	/**
	 * 
	 * @param service the PlaybackService currently binded to the Activity or null
	 */
	public void setService(PlaybackService service) {
		mService = service;
		
		if (service != null) {
			Log.i("FragmentPlayer", "Setting service");
			restoreState();
		}
		else
			Log.i("FragmentPlayer", "Unsetting service: instance was null");
			
	}

	/**
	 * Update the playlist to play
	 * @param plName the name of the playlist to play: should be the m3u file without extension
	 */
	public void setPlaylist(String plName) {
		if (asyncRefresh != null) {
			asyncRefresh.cancel(true);
			listAdapter.notifyDataSetChanged();
		}
		
		playlistName = plName;
		isDirectory = false;
		
		//Stop playback if it was playing
		if (mService != null) {
			mService.stop();
			currentIndex = -1;
		}
		
		//Update UI configuration
		startButton.setImageResource(R.drawable.ic_media_play);
		albumArt.setImageResource(R.drawable.def_album_cover);
		setButtonsEnabled(false);
		startButton.setEnabled(true);
		playlistIsSettled = false;
		
		refreshList();
	}
	
	/**
	 * Set a directory to play
	 * @param folder path to the folder to play
	 */
	public void setDirectory(String folder) {
		Log.i("FragmentPlayer", "Setting folder: " + folder);

		if (asyncRefresh != null) {
			asyncRefresh.cancel(true);
			listAdapter.notifyDataSetChanged();
		}
		
		playlistName = folder;
		isDirectory = true;

		//Stop playback if it was playing
		if (mService != null) {
			mService.stop();
			currentIndex = -1;
		}
		
		//Update UI configuration
		startButton.setImageResource(R.drawable.ic_media_play);
		albumArt.setImageResource(R.drawable.def_album_cover);
		setButtonsEnabled(false);
		startButton.setEnabled(true);
		playlistIsSettled = false;
		
		refreshList();
	}
	
	private void refreshList() {

		Log.i("FragmentPlayer", "Called refreshList() method");
		
		list.setVisibility(View.INVISIBLE);
		progress.setVisibility(View.VISIBLE);
		
		asyncRefresh = new refreshListAsync();
		asyncRefresh.execute();
		
	}
	
	private class refreshListAsync extends AsyncTask<Void, Boolean, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			Log.i("FragmentPlayer", "refreshList started");

			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
			
			//1. RAW LOAD
			Log.i("FragmentPlayer", "refreshList raw load");
			
			//Retrieve a list of paths
			if (mService != null && playlistIsSettled) {
				Log.i("FragmentPlayer", "Loading playlist directly from service");
				songsPath = (ArrayList<String>)mService.getPlaylist();
			}
			else {
				if (isDirectory) {
					Log.i("FragmentPlayer", "Loading a directory");
					songsPath = PlayListManager.getDirectoryPlaylist(playlistName);
				}
				else {
					String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
					songsPath = PlayListManager.getSongsString(plPath, playlistName);
				}
			}
			
			if (songsPath.size() == 0) {
				playlistName = null;
			}
			
			//Construct the HashMaps for ListView
			ArrayList<HashMap<String, String>> newPl = new ArrayList<HashMap<String, String>>();
			for (int i = 0; i < songsPath.size(); i++) {
				String songPath = songsPath.get(i);
				HashMap<String, String> song = new HashMap<String, String>();
				song.put(SongAdapter.KEY_TITLE, songPath.substring(songPath.lastIndexOf("/")+1));
				
				newPl.add(song);
			}
			playlist = newPl;
			

			
			publishProgress(true);
			
			
			
			//2. LAZY LOAD
			Log.i("FragmentPlayer", "refreshList lazy load");
			
			//Update song by song all ID3 fields
			for (int i = 0; i < songsPath.size(); i++) {
				String songPath = songsPath.get(i);
				HashMap<String, String> song = playlist.get(i);
				if(Id3Tag.getTitle(songPath) != null){
					song.put(SongAdapter.KEY_TITLE, Id3Tag.getTitle(songPath));
				}
				else{
					song.put(SongAdapter.KEY_TITLE, songPath.substring(songPath.lastIndexOf("/")+1));
				}
				if(Id3Tag.getArtist(songPath) != null){
					song.put(SongAdapter.KEY_ARTIST, Id3Tag.getArtist(songPath));
				}
				else{
					song.put(SongAdapter.KEY_ARTIST, getString(R.string.song_adapter_def_title));
				}
				song.put(SongAdapter.KEY_DURATION, Id3Tag.getLength(songPath));
				//song.put(SongAdapter.KEY_THUMB, Id3Tag.getAlbumPath(getActivity(), songPath));
				song.put(SongAdapter.KEY_PLAYING, "");
				
				publishProgress(false);
			}
			
			
			
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Boolean... values) {
			super.onProgressUpdate(values);
			
			if (values[0] == true) {
				//Mid stage between raw load and lazy load
				listAdapter.notifyDataSetInvalidated();
				
				listAdapter = new SongAdapter(FragmentPlayer.this.getActivity(), playlist);
				
				list.setAdapter(listAdapter);
				
				listAdapter.notifyDataSetChanged();
				
				list.setVisibility(View.VISIBLE);
				progress.setVisibility(View.INVISIBLE);
			}
			else
				listAdapter.notifyDataSetChanged();
			
		}
		
		@Override
		protected void onPostExecute(Void result) {
			
			super.onPostExecute(result);

			listAdapter.notifyDataSetChanged();
			
			if (firstLoading) {
				firstLoading = false;
				//Used to set play or pause symbol on the current playing song, if any
				updateAdapters(-1, currentIndex);
			}
			
		}
	}
	
	private Runnable seekRunnable = new Runnable() {

		@Override
		public void run() {
			int seekMax = 0;
			try {
				Log.i("FragmentPlayer", "Started Runnable on SeekBar");
				while (true) {
					Thread.sleep(1000);
					
					int d = mService.getDuration();
					if (seekMax != d) {
						//Current song duration has changed, update the length of the seek bar
						seekMax = d;
						seekBar.setMax(seekMax);
						Log.i("FragmentPlayer", "Runnable on SeekBar has changed length of the bar");
					}
					if (seekMax > 0) {
						seekBar.setProgress(mService.getPosition());
					}
				}
			} catch (InterruptedException e) {
				Log.i("FragmentPlayer", "Interrupted Runnable on SeekBar");
			}
		}
		
	};
	
	private void addLineRenderer() {
		
		//Set parameters for the visualizer graph
		Paint linePaint = new Paint();
		linePaint.setStrokeWidth(5f);
		linePaint.setAntiAlias(true);
		linePaint.setColor(Color.argb(150, 0, 128, 255));

		Paint lineFlashPaint = new Paint();
		lineFlashPaint.setStrokeWidth(5f);
		lineFlashPaint.setAntiAlias(true);
		lineFlashPaint.setColor(Color.argb(200, 255, 255, 255));
		LineGraphRenderer lineRenderer = new LineGraphRenderer(linePaint, lineFlashPaint, true);
		graph.addRenderer(lineRenderer);
		
	}

}
