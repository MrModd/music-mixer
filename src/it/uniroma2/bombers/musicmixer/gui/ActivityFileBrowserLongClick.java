package it.uniroma2.bombers.musicmixer.gui;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import it.uniroma2.bombers.musicmixer.Id3Tag;
import it.uniroma2.bombers.musicmixer.R;


public class ActivityFileBrowserLongClick extends Activity{

	public static final String TAG_INTENT = "path";
	public static final String TAG_LOG = "Id3Modifier";
	
	private String path;
	private String title;
	private String artist;
	private String genre;
	private String year;
	private String length;
	private String thumbPath;
	
	EditText nameEdit;
	EditText titleEdit;
	EditText artistEdit;
	EditText genreEdit;
	EditText yearEdit;
	EditText lengthEdit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_browser_long_click);
		
		Intent i = getIntent();
		path = i.getStringExtra(TAG_INTENT);
		title = Id3Tag.getTitle(path);
		artist = Id3Tag.getArtist(path);
		genre = Id3Tag.getGenre(path);
		year = Id3Tag.getYear(path);
		length = Id3Tag.getLength(path);		
		thumbPath = Id3Tag.getAlbumPath(this, path);
		
		File f = new File (path);
		
		ImageView iv = (ImageView)findViewById(R.id.activity_file_browser_long_click_thumb);
		nameEdit = (EditText)findViewById(R.id.activity_file_browser_long_click_filename_edit);
		titleEdit = (EditText)findViewById(R.id.activity_file_browser_long_click_title_edit);
		artistEdit = (EditText)findViewById(R.id.activity_file_browser_long_click_artist_edit);
		genreEdit = (EditText)findViewById(R.id.activity_file_browser_long_click_genre_edit);
		yearEdit = (EditText)findViewById(R.id.activity_file_browser_long_click_year_edit);
		lengthEdit = (EditText)findViewById(R.id.activity_file_browser_long_click_length_edit);
		
		if(thumbPath != null){
			File tf = new File(thumbPath);
			Bitmap myBitmap = BitmapFactory.decodeFile(tf.getAbsolutePath());
			iv.setImageBitmap(myBitmap);
		}
		else{
			iv.setImageResource(R.drawable.def_album_thumb);
		}
		
		nameEdit.setHint(f.getName());
		
		if(title != null){
			titleEdit.setHint(title);
		}
		else{
			titleEdit.setHint(getString(R.string.title_title_id3editor_def));
		}
		
		if(artist != null){
			artistEdit.setHint(artist);
		}
		else{
			artistEdit.setHint(getString(R.string.title_artist_id3editor_def));
		}
		
		if(genre != null){
			genreEdit.setHint(genre);
		}
		else{
			genreEdit.setHint(getString(R.string.title_genre_id3editor_def));
		}
		
		if(year != null){
			yearEdit.setHint(year);
		}
		else{
			yearEdit.setHint(getString(R.string.title_year_id3editor_def));
		}
		
		if(length != null){
			lengthEdit.setHint(length);
		}
		else{
			lengthEdit.setHint(getString(R.string.title_length_id3editor_def));
		}
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	@Override
	public void onBackPressed() {
		Intent returnIntent = new Intent();
		setResult(RESULT_CANCELED,returnIntent);
		finish();
	}
}
