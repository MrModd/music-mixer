package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DialogPlaylistOptionsMenu extends DialogFragment {
	
	private String playlistName = null;
	private Button deleteButton;
	private Button renameButton;
	private Button modifyButton;
	
    // Use this instance of the interface to deliver action events
	DialogPlaylistOptionsMenuListener mListener;
	
	/* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface DialogPlaylistOptionsMenuListener {
    	public void onRequestDialogDelete(DialogFragment dialog);
        public void onRequestDialogRename(DialogFragment dialog);
        public void onRequestDialogModify(DialogFragment dialog);
    }
	
	public void setOnRequestDialogListener(DialogPlaylistOptionsMenuListener listener) {
		mListener = listener;
	}

	public void setPlaylistName(String name) {
		playlistName = name;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();
		
		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		View v = inflater.inflate(R.layout.dialog_playlist_options_menu, null);
		builder.setView(v);
		
		deleteButton = (Button)v.findViewById(R.id.dialogPlaylistOptionDelete);
		renameButton = (Button)v.findViewById(R.id.dialogPlaylistOptionRename);
		modifyButton = (Button)v.findViewById(R.id.dialogPlaylistOptionModify);
		
		//Add elements in the dialog box
		if (playlistName != null) {
			builder.setTitle(playlistName);
		}
		else {
			builder.setTitle(R.string.dialog_playlist_option_title);
		}
		
		//Listeners
		deleteButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (playlistName != null) {
					if (mListener != null) {
						mListener.onRequestDialogDelete(DialogPlaylistOptionsMenu.this);
					}
					dismiss();
				}
			}
			
		});
		
		//Listeners
		renameButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (playlistName != null) {
					if (mListener != null) {
						mListener.onRequestDialogRename(DialogPlaylistOptionsMenu.this);
					}
					dismiss();
				}
			}
			
		});
		
		//Listeners
		modifyButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (playlistName != null) {
					if (mListener != null) {
						mListener.onRequestDialogModify(DialogPlaylistOptionsMenu.this);
					}
					dismiss();
				}
			}
			
		});
		
		return builder.create();
	}

}
