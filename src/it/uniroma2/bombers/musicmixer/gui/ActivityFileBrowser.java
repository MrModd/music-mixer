package it.uniroma2.bombers.musicmixer.gui;

import it.uniroma2.bombers.musicmixer.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ActivityFileBrowser extends Activity {
	
	public static final String result = "result";
	public static final String exceptionText = "Permission denied";
	public static final String CURRENT_DIR_FILE_PICKER_PREF = "Current directory";
	public static final String TAG_LOG = "FilePicker";
	public static final int requestCodeForResult = 50;
	
	private ArrayList<File> directoryListing;
	FileAdapter fa; 						
	String actualPath;				
	String[] formats = {"mp3", "3gp", "wma", "flac", "3gpp"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_browser);
		
		
		actualPath = PreferenceManager.getDefaultSharedPreferences(this).getString(CURRENT_DIR_FILE_PICKER_PREF, "/");
		
		ListView lv = (ListView)findViewById(R.id.content_list);
		directoryListing = new ArrayList<File>();
		File f = new File(actualPath);
		
		if(!f.exists()){
			actualPath="/";
			f = new File(actualPath);
			PreferenceManager.getDefaultSharedPreferences(ActivityFileBrowser.this).edit().putString(CURRENT_DIR_FILE_PICKER_PREF, actualPath).commit();
		}
		
		//Listing of the root directory or the last directory visited
		try {
			directoryListing = listing(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		fa = new FileAdapter(this, directoryListing);
		lv.setAdapter(fa);
		
		//Listener on click to a file into the directory
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				//If the selection is a file, return it to the parent activity
				if(directoryListing.get(arg2).isFile()){
					Intent returnIntent = new Intent();
					try {
						returnIntent.putExtra(result,directoryListing.get(arg2).getCanonicalPath());
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					PreferenceManager.getDefaultSharedPreferences(ActivityFileBrowser.this).edit().putString(CURRENT_DIR_FILE_PICKER_PREF, actualPath).commit();
					
					setResult(RESULT_OK,returnIntent);
					finish();
				}
				//The selection is a directory -> listing
				else{
					ArrayList<File> temp = null;
					try {
						actualPath = directoryListing.get(arg2).getCanonicalPath();
						temp = listing(directoryListing.get(arg2));
					} catch (IOException e) {
						return;
					}
					directoryListing.clear();
					directoryListing.addAll(temp);
					fa.notifyDataSetChanged();
				}
			}
			
		});
		
		//Listener for the longClick to a file
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				File selected = directoryListing.get(arg2);
				//If the item is a file call the activity of the tags
				if(selected.isFile()){
					String[] name = selected.getName().split("\\.");
					//If no extension, don't include
					if(name.length>=2 && !selected.isHidden()){
						boolean ctrl = false;
						for(int j=0; j<formats.length; j++){
							if(name[name.length-1].equals(formats[j])){
								ctrl=true;
								j=formats.length;
							}
						}
						if(ctrl){
							try {
								callId3Modifier(selected.getCanonicalPath());
							} catch (IOException e) {
								Log.e(TAG_LOG, "Error long clicking file");
							}
						}
					}
				}
				return true;
			}
		});
	}
	
	/**
	 * Call the activity for the tags
	 * @param path
	 */
	protected void callId3Modifier(String path){
		Intent i = new Intent(this, ActivityFileBrowserLongClick.class);
		i.putExtra(ActivityFileBrowserLongClick.TAG_INTENT, path);
		startActivityForResult(i, requestCodeForResult);
	}
	
	/**
	 * Function for the directory listing
	 * @param f
	 * @return The file into the directory f
	 * @throws IOException
	 */
	private ArrayList<File> listing(File f) throws IOException{
		ArrayList<File> collection = new ArrayList<File>();
		
		//If the directory isn't the root directory, add to the listing the .. directory
		if(f.getCanonicalPath().length()>1){
			File previosDir = new File(f.getCanonicalPath()+"/..");
			collection.add(previosDir);
		}
		
		File[] listing = f.listFiles();
		//If listing == null, permission denied
		if(listing==null){
			CharSequence text = getResources().getString(R.string.error_explorer);
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
			throw new IOException(exceptionText);     
		}
		
		for(int i = 0; i<listing.length; i++){
			File tmp = listing[i];
			listing[i]=tmp.getCanonicalFile();
		}
		
		Arrays.sort(listing);
		collection.addAll(Arrays.asList(listing));
		//Filtering all the not-audio files
		filter(collection);
		
		return collection;
	}
	
	private void filter(ArrayList<File> list){
		for(int i=0;i<list.size();i++){
			File tmp = list.get(i);
			//Directories are allowed. Else continue
			if(!tmp.isDirectory()){
				//Getting all the files extensions
				String[] name = tmp.getName().split("\\.");
				//If no extension, remove
				if(name.length<2 || tmp.isHidden()){
					list.remove(i);
					i--;
				}
				else{
					//If supported format, keep the file. Otherwise remove 
					boolean ctrl = false;
					for(int j=0; j<formats.length; j++){
						if(name[name.length-1].equals(formats[j])){
							ctrl=true;
						}
					}
					if(!ctrl){
						list.remove(i);
						i--;
					}
				}
			}
		}
	}
	
	
	//Override of the onBack method to permit the navigation into the parent directory
	@Override
	public void onBackPressed() {
	   if(actualPath.length()<=1){
		   super.onBackPressed();
		   return;
	   }
	   ArrayList<File> temp = null;
	   File f = null;
		try {
			f = (new File(actualPath+"/..")).getAbsoluteFile();
			actualPath = f.getCanonicalPath();
			temp = listing(f);
		} catch (IOException e) {
			return;
		}
		directoryListing.clear();
		directoryListing.addAll(temp);
		fa.notifyDataSetChanged();
	}
	
	/**
	 * Function called on cancel pressed
	 * @param v
	 */
	public void goBack(View v){
		Intent returnIntent = new Intent();
		setResult(RESULT_CANCELED,returnIntent);
		finish();
	}

}