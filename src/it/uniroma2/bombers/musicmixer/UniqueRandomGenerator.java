package it.uniroma2.bombers.musicmixer;

import java.util.ArrayList;
import java.util.Random;

import android.util.Log;

/**
 * This class implements Generator interface methods
 * in order to uniquely generate random numbers in range
 * [0, MAX).
 *
 * @author Giulio Picierro
 */
public class UniqueRandomGenerator implements Generator {
	
	private int[] randomArray;
	private int cur;
	
	private void generateRandom() {
		Random rnd = new Random();
		ArrayList<Integer> array = new ArrayList<>(randomArray.length);
		
		StringBuilder sb = new StringBuilder();
		
		for (int i=0; i < randomArray.length; i++) {
			array.add(i);
		}

		for (int i=0; i < randomArray.length; i++) {
			int index = rnd.nextInt(array.size());
			
			randomArray[i] = array.remove(index);
			
			sb.append(randomArray[i] + " ");
		}
		
		Log.i(PlaybackService.TAG, "Random index generated: ["+sb.toString()+"]");
	}
	
	public UniqueRandomGenerator(int to) {
		
		this.randomArray = new int[to];
		this.cur = 0;
		
		generateRandom();
	}

	@Override
	public int get() {
		if (cur < randomArray.length) {
			return randomArray[cur];
		}
		else {
			return randomArray.length;
		}
	}

	@Override
	public int next() {
		if (cur + 1 < randomArray.length) {
			return randomArray[++cur];
		}
		else {
			cur = randomArray.length;
			return randomArray.length;
		}
	}

	@Override
	public int prev() {
		cur = Math.max(0, cur-1);
		
		return randomArray[cur];
	}

	@Override
	public void set(int id) {
		
		if (id < 0 || id >= randomArray.length)
			throw new IllegalArgumentException();
		
		for (int i=0; i < randomArray.length; i++) {
			if (randomArray[i] == id) {
				cur = i;
				
				// Log.i(PlaybackService.TAG, "set id " + id + ", cursor: " + i);
				
				break;
			}
		}
	}

	@Override
	public void reset() {
		generateRandom();
		
		cur = 0;
	}

	@Override
	public final int max() {
		return randomArray.length;
	}

	public void setTop(int id) {
		// search the element with "id" and swaps with first
		for (int i=0; i < randomArray.length; i++) {

			if (randomArray[i] == id) {
				int temp = randomArray[0];
				randomArray[0] = randomArray[i];
				randomArray[i] = temp;

				break;
			}
		}
	}
}
