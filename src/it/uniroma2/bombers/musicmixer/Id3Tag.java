package it.uniroma2.bombers.musicmixer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Log;

public class Id3Tag {
	
	public static final String THUMBNAIL_FOLDER = "/thumbnails";
	public static final  String TAG_LOG = "Id3";
	
	/**
	 * Get the title tag of the file at that path
	 * @param path
	 * @return the title of the song, null if not present
	 */
	public static String getTitle(String path){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);
		String ret = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
		mmr.release();
		return ret;
	}
	
	/**
	 * Get the title tag of the file at that Uri
	 * @param ctx
	 * @param uri
	 * @return The title of the song, null if not present
	 */
	public static String getTitle(Context ctx, Uri uri){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(ctx, uri);
		String ret = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
		mmr.release();
		return ret;
	}
	
	/**
	 * Get the artist tag of the file at that path
	 * @param path
	 * @return The artist of the song, null if not present
	 */
	public static String getArtist(String path){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);
		String ret = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
		mmr.release();
		return ret;
	}
	
	/**
	 * Get the artist tag of the file at that Uri
	 * @param uri
	 * @return The artist of the song, null if not present
	 */
	public static String getArtist(Context ctx, Uri uri){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(ctx, uri);
		String ret = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
		mmr.release();
		return ret;
	}
	
	/**
	 * Get the length tag of the file at that path, in millis
	 * @param path
	 * @return The length of the song in millis
	 */
	public static long getLengthInMillis(String path){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);
		long ret = Long.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
		mmr.release();
		return ret;
	}
	
	/**
	 * Get the length tag of the file at that Uri, in millis
	 * @param uri
	 * @return The length of the song in millis
	 */
	public static long getLengthInMillis(Context ctx, Uri uri){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(ctx, uri);
		long ret = Long.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
		mmr.release();
		return ret;
	}
	
	/**
	 * Get the length tag of the file at that path, as a formatted String
	 * @param path
	 * @return The length of the song as a formatted String
	 */
	public static String getLength(String path){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);
		String ret = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
		mmr.release();
		
		SimpleDateFormat formatter = new SimpleDateFormat("mm:ss", Locale.getDefault());
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(Long.parseLong(ret));
	    return formatter.format(calendar.getTime());
	}
	
	/**
	 * Get the length tag of the file at that Uri, as a formatted String
	 * @param uri
	 * @return The length of the song as a formatted String
	 */
	public static String getLength(Context ctx, Uri uri){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(ctx, uri);
		String ret = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
		mmr.release();
		
		SimpleDateFormat formatter = new SimpleDateFormat("mm:ss", Locale.getDefault());
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(Long.parseLong(ret));
	    return formatter.format(calendar.getTime());
	}
	
	/**
	 * Get the path to the album art of the song at the path given
	 * @param ctx
	 * @param path
	 * @return The path of the album art
	 */
	public static String getAlbumPath(Context ctx, String path){
		String conc = path;
		int  hash = conc.hashCode();
		File thumbDir = new File(ctx.getCacheDir()+THUMBNAIL_FOLDER);
		if(!thumbDir.exists()){
			if(!thumbDir.mkdir()){
				Log.e("Thumbnail", "Error creating directory in cache");
				return null;
			}
		}
		
		File thumb = new File(thumbDir.getAbsolutePath(), Integer.toString(hash)+".jpg");
		
		if(thumb.exists()){
			return thumb.getAbsolutePath();
		}
		
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);
		byte[] stream = mmr.getEmbeddedPicture();
		if(stream == null){
			return null;
		}
		mmr.release();
		try {
			FileOutputStream fos = new FileOutputStream(thumb);
			fos.write(stream);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return thumb.getAbsolutePath();
	}
	
	/**
	 * Get the path to the album art of the song at the Uri given
	 * @param ctx
	 * @param uri
	 * @return The path of the album art
	 */
	public static String getAlbumPath(Context ctx, Uri uri){
		String conc = uri.getPath();
		int  hash = conc.hashCode();
		File thumbDir = new File(ctx.getCacheDir()+THUMBNAIL_FOLDER);
		if(!thumbDir.exists()){
			if(!thumbDir.mkdir()){
				Log.e("Thumbnail", "Error creating directory in cache");
				return null;
			}
		}
		
		File thumb = new File(thumbDir.getAbsolutePath(), Integer.toString(hash)+".jpg");
		
		if(thumb.exists()){
			return thumb.getAbsolutePath();
		}
		
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(ctx, uri);
		byte[] stream = mmr.getEmbeddedPicture();
		if(stream == null){
			return null;
		}
		mmr.release();
		
		try {
			FileOutputStream fos = new FileOutputStream(thumb);
			fos.write(stream);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return thumb.getAbsolutePath();
	}
	
	/**
	 * Compute the sample size of the image
	 * @param options
	 * @param reqWidth
	 * @param reqHeight
	 * @return return the sample size
	 */
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	
	    return inSampleSize;
	}
	
	/**
	 * Get the bitmap thumbnail for the image at path
	 * @param path
	 * @param reqWidth
	 * @param reqHeight
	 * @return the bitmap thumbnail
	 */
	public static Bitmap getThumbnailBitmap(String path, int reqWidth, int reqHeight) {
		
	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    //BitmapFactory.decodeResource(res, resId, options);
	    BitmapFactory.decodeFile(path, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeFile(path, options);
	}
	
	/**
	 * Get genre from the file tags
	 * @param path
	 * @return return the genre code
	 */
	public static String getGenre(String path){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);
		String ret = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
		mmr.release();
		return ret;
	}
	
	/**
	 * Get the year from the file tags
	 * @param path
	 * @return the year of the song
	 */
	public static String getYear(String path){
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);
		String ret = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR);
		mmr.release();
		return ret;
	}

}
