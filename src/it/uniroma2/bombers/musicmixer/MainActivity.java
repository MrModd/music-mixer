package it.uniroma2.bombers.musicmixer;

import it.uniroma2.bombers.musicmixer.RecorderService.LocalBinder;
import it.uniroma2.bombers.musicmixer.PlaybackService.PlaybackBinder;
import it.uniroma2.bombers.musicmixer.gui.ActivityDirectoryBrowser;
import it.uniroma2.bombers.musicmixer.gui.ActivityManagePlaylists;
import it.uniroma2.bombers.musicmixer.gui.ActivitySettings;
import it.uniroma2.bombers.musicmixer.gui.DialogChooseRadioStream;
import it.uniroma2.bombers.musicmixer.gui.DialogChooseRadioStream.NoticeDialogListener;
import it.uniroma2.bombers.musicmixer.gui.FragmentPlayer;
import it.uniroma2.bombers.musicmixer.gui.FragmentRadio;
import it.uniroma2.bombers.musicmixer.gui.FragmentRecorder;
import it.uniroma2.bombers.musicmixer.gui.FragmentSettings;

import java.util.ArrayList;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ShareActionProvider;

public class MainActivity extends Activity {
	public static final String PLAYER_FRAGMENT_TAG = "player fragment";
	public static final String RECORDER_FRAGMENT_TAG = "recorder fragment";
	public static final String RADIO_FRAGMENT_TAG = "radio fragment";
	public static final String DIALOG_RADIO_STREAM_TAG = "dialog radio stream";
	public static final Integer ACTIVITY_DIRECTORY_PICKER_TAG = 24;
	
	private ShareActionProvider shareButton;
	
	private DrawerLayout drawer;
	private ListView drawerList;
	private ArrayAdapter<String> drawerListAdapter;
	private ActionBarDrawerToggle drawerToggle;

	private int fragmentState;	//Keep track of current shown fragment:
								// 0: FragmentPlayer
								// 1: FragmentRecorder
								// 2: FragmentRadio
	
	private FragmentPlayer playerFragment;
	private FragmentRecorder recorderFragment;
	private FragmentRadio radioFragment;
	
	private Intent recorderIntent;
	private RecorderService recorder;
	
	private Intent playerIntent;
	private PlaybackService player;
	
	private Intent shareIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		drawerList = (ListView)findViewById(R.id.left_drawer);
		drawer = (DrawerLayout)findViewById(R.id.drawer_layout);

		drawerListAdapter = new ArrayAdapter<String>(
				this,
				R.layout.drawer_list_adapter,
				R.id.drawer_list_element);
		
		//Enable drawer
		drawerList.setAdapter(drawerListAdapter);
		drawerToggle = new ActionBarDrawerToggle(
				this,
				drawer,
				R.drawable.ic_drawer,
				R.string.content_description_drawer_open,
				R.string.content_description_drawer_close);
		
		// Set the drawer toggle as the DrawerListener
        drawer.setDrawerListener(drawerToggle);
        
        //Enable opening drawer with app icon
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		//Construct fragment objects
		playerFragment = new FragmentPlayer();
		recorderFragment = new FragmentRecorder();
		radioFragment = new FragmentRadio();
		//The constructor of these objects calls the OnCreateView() method
		//even if they are not inflated...

		//Create recorder and player services
		recorderIntent = new Intent(this, RecorderService.class);
		startService(recorderIntent);
		bindService(recorderIntent, recorderConnection, Context.BIND_AUTO_CREATE);
		
		playerIntent = new Intent(this, PlaybackService.class);
		startService(playerIntent);
		bindService(playerIntent, playerConnection, 0);
		
		//Create the default UI
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.activity_main_container, playerFragment, PLAYER_FRAGMENT_TAG);
		ft.commit();
		fm.executePendingTransactions();
		fragmentState = 0;
		
		
		
		//Listeners
		
		fm.addOnBackStackChangedListener(new OnBackStackChangedListener() {
			
			@Override
			public void onBackStackChanged() {
				//When back button pressed update fragmentState to return to FragmentPlayer configuration
				Log.i("MainActivity", "Back button pressed");
				fragmentState = 0;
			}
		});
		
		drawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				drawer.closeDrawers();
				
				if (position == drawerList.getCount() - 4) {
					//Manage playlists
					
					Log.i("MainActivity", "Element on drawer selected: manage playlists");
					startActivity(new Intent(MainActivity.this, ActivityManagePlaylists.class));
					
				}
				else if (position == drawerList.getCount() - 3) {
					//Play a folder

					Log.i("MainActivity", "Element on drawer selected: selected play a folder, starting folder picker activity");
					startActivityForResult(new Intent(MainActivity.this, ActivityDirectoryBrowser.class), ACTIVITY_DIRECTORY_PICKER_TAG);
					
				}
				else if (position == drawerList.getCount() - 2) {
					//Radio stream

					Log.i("MainActivity", "Element on drawer selected: selected radio stream");
					
					SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
					
					if (player != null && player.isPlaying() && pref.getBoolean(FragmentRadio.PREF_KEY_RADIO_SELECTED, false)) {
						//If is already playing a radio stream don't show dialog message
						Log.i("MainActivity", "Radio is playing, jumping directly to radio fragment");
						setConfiguration(2);
						
					}
					else {
						
						Log.i("MainActivity", "Radio is not playing, showing dialog message");
						
						DialogChooseRadioStream radioStream = new DialogChooseRadioStream();
						radioStream.show(getFragmentManager(), DIALOG_RADIO_STREAM_TAG);
						radioStream.setOnDialogClickListener(new NoticeDialogListener () {
	
							@Override
							public void onDialogPositiveClick(DialogFragment dialog, String url) {
								
								setConfiguration(2);
								radioFragment.setUrl(url);
							}
	
							@Override
							public void onDialogNegativeClick(DialogFragment dialog) {
								//Do nothing
							}
							
						});
						
					}
					
				}
				else if (position == drawerList.getCount() - 1) {
					//Recorder

					Log.i("MainActivity", "Element on drawer selected: starting recorder fragment");
					setConfiguration(1);
					
				}
				else {
					//Play a playlist

					Log.i("MainActivity", "Element on drawer selected: starting player fragment and setting playlist");
					setConfiguration(0);
					playerFragment.setPlaylist(drawerListAdapter.getItem(position));
					
				}
			}
			
		});
	}
	
	@Override
	protected void onStart() {
		
		super.onStart();

		//Populate drawer with playlists
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		String plPath = pref.getString(getString(R.string.pref_key_player_playlists_path), FragmentSettings.DEF_PATH_PLAYLISTS);
		
		ArrayList<String> playlists = PlayListManager.getPlayList(plPath);
		drawerListAdapter.clear();
		drawerListAdapter.addAll(playlists);
		drawerListAdapter.addAll(getResources().getStringArray(R.array.drawer_static_entry));
		
		drawerListAdapter.notifyDataSetChanged();
		
	}
	
	@Override
	protected void onDestroy() {

		Log.i("MainActivity", "Destroying MainActivity");
		
		//Unbind services
		unbindService(recorderConnection);
		if(!recorder.status()){
			stopService(recorderIntent);
		}
		unbindService(playerConnection);
		if(!player.isPlaying() && !player.isPaused()){
			stopService(playerIntent);
		}
		
		super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_share, menu);
		
		//Add ShareIntent for share button
		MenuItem item = menu.findItem(R.id.action_share);
		
		//Set share button action and create share intent
		shareButton = (ShareActionProvider)item.getActionProvider();
		if (shareButton != null) {
			shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType("text/plain");
			String message = getResources().getString(R.string.action_share_song_message_default);
		    shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
			shareButton.setShareIntent(shareIntent);
			
			// KNOWN BUG: Facebook APIs don't handle standard EXTRA_SUBJECT and EXTRA_TEXT
			// field of the sharing intent
			// https://developers.facebook.com/bugs/332619626816423
		}
		
		return true;
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		int id = item.getItemId();
		if (id == R.id.action_settings_share_menu) {
			// Start Preferences activity
			startActivity(new Intent(this, ActivitySettings.class));
			return true;
		}
		else if(drawerToggle.onOptionsItemSelected(item)) {
			//Open the drawer
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == ACTIVITY_DIRECTORY_PICKER_TAG && resultCode == RESULT_OK) {
			
			String plPath = data.getExtras().getString(ActivityDirectoryBrowser.result);
			
			Log.i("MainActivity", "Element on drawer selected: folder picker activity returned, starting player fragment and setting directory path");
			
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
			pref.edit()
				.putString(FragmentPlayer.PREF_KEY_PLAYER_PLNAME, null)
				.commit();
			
			setConfiguration(0);
			playerFragment.setDirectory(plPath);
			
		}
	}

	//Methods for drawer
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		drawerToggle.syncState();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}
	
	
	
	/**
	 * Change shown fragmend
	 * @param configuration 0 = player, 1 = recorder, 2 = radio
	 */
	private void setConfiguration(int configuration) {
		Log.i("MainActivity", "Changing fragment configuration: was " + fragmentState + " and now is " + configuration);
		
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		
		if (configuration == 0) {
			//FragmentPlayer
			if (fragmentState != 0) {
				Log.i("MainActivity", "Returning to player fragment");
				fm.popBackStackImmediate();
			}
		} else if (configuration == 1) {
			//FragmentRecorder
			if (fragmentState != 1) {
				if (fragmentState != 0) {
					fm.popBackStackImmediate();
				}
				Log.i("MainActivity", "Replacing player fragment with recorder fragment");
				ft.replace(R.id.activity_main_container, recorderFragment, RECORDER_FRAGMENT_TAG);
				ft.addToBackStack(null);
			}
		} else if (configuration == 2) {
			//FragmentRadio
			if (fragmentState != 2) {
				if (fragmentState != 0) {
					fm.popBackStackImmediate();
				}
				Log.i("MainActivity", "Replacing player fragment with radio fragment");
				ft.replace(R.id.activity_main_container, radioFragment, RADIO_FRAGMENT_TAG);
				ft.addToBackStack(null);
			}
		}

		ft.commit();
		
		fm.executePendingTransactions();
		
		fragmentState = configuration;
	}
    
	/**
	 * Set message for share intent
	 * @param song message to show when share intent is called from the UI
	 */
    public void setSong(String song){
    	if (shareIntent != null) {
	    	shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, song);
	    	shareButton.setShareIntent(shareIntent);
    	}
    }
	
	/** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection recorderConnection = new ServiceConnection() {

		// TODO Auto-generated method stub
        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LocalBinder binder = (LocalBinder) service;
            recorder = binder.getService();
            recorderFragment.setRecorder(recorder);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        	Log.i("MainActivity", "Unbinding recorder service");
    		//Unsetting services
        	recorderFragment.setRecorder(null);
        	recorder = null;
        }
    };
    private ServiceConnection playerConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            PlaybackBinder binder = (PlaybackBinder) service;
            player = binder.getService();
            playerFragment.setService(player);
            radioFragment.setService(player);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        	Log.i("MainActivity", "Unbinding player service");
        	playerFragment.setService(null);
        	radioFragment.setService(null);
        	player = null;
        }
    };
}
