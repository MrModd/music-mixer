package it.uniroma2.bombers.musicmixer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


/**
 * This class survey static methods to manage m3u file
 *  
 * 
 *
 * @author Stefano Di Francescangelo
 */


public class PlayListManager {

	private static final String[] formats = {"mp3", "3gp", "wma", "flac", "3gpp"};

	/**
	 * Every line in an M3U file is either a playlist comment, a blank, or a file or stream source.

	 *	A comment line begins with the pound sign, #. Blanks are ignored.
	 *
	 *	A source is the address of a media file or stream. A source address can be anything the M3U reader is capable of understanding. These include absolute file paths and URLs.
	 *
	 *	To add more entries, simply repeat the #EXTINF and http:// line(s) that correspond to the type of entry (stream or media file) you want to add.

	   #EXTM3U at the first line of the file
	   #EXTINF:time,Artist Name - Track Title 
	   path_of_file/address of streaming


	   time=-1 for streaming file
	 */



	/**
	 * control the state of every playlist in tha path 
	 * 
	 * */

	public static void refreshPlaylist	(String path){
		ArrayList<String> playlists=getPlayList(path);
		for(String playlist: playlists){
			//while(!refreshPlaylist(path, playlist));
			refreshPlaylist(path, playlist);
		}
	}
	
	/**
	 * control the state of a single playlist file, 
	 * @return the succes of refresh file
	 * */
	public static Boolean refreshPlaylist( String path,String playlist){

			File inputFile = new File(path+playlist+".m3u");
		File tempFile = new File(path+playlist+"temp.m3u");
		Boolean must_find_song=false; //find song's info

		BufferedReader reader;
		String prec_lines = null;
		boolean successful = false;
		try {
			reader = new BufferedReader(new FileReader(inputFile));

			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

			String currentLine;
			Boolean first_line=true;
			while((currentLine = reader.readLine()) != null) {
				
				if ( first_line){
					writer.write("#EXTM3U\n");
					first_line=false;
				}

				else  if (currentLine.contains("#EXTINF")){ //nuovo file nella playlist


					must_find_song=true;

					prec_lines=currentLine;

				}


				
				else if(!currentLine.isEmpty() && must_find_song){ // se non è vuota e se non è un uri sarà il path ad un file in locale

					if(currentLine.contains("http")){ 
						if(exists_uri(currentLine) ){
							writer.write(prec_lines+"\n");
							writer.write(currentLine+"\n");
						}

					}
					else if(new File (currentLine).exists()){
						writer.write(prec_lines+"\n");
						writer.write(currentLine+"\n");
					}

					must_find_song=false;
				}	

			}   

			writer.flush();
			successful = tempFile.renameTo(inputFile);
			writer.close();
			reader.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
			return false;
		} catch (IOException e) {
		
			e.printStackTrace();
			return false;
		}

		return successful;



	}

	/**
	 * return the title of every playlist in path
	 * @return ArrayList that contain the titles
	 * */
	public static ArrayList <String> getPlayList(String path){
		ArrayList<String> result=new ArrayList<String>();

		File folder = new File(path);
		if(!folder.exists()){
			folder.mkdir();
		}
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			File temp=listOfFiles[i];
			if (temp.isFile() && temp.getName().contains(".m3u")) {
				//in questo modo dovrebbe tornare senza .m3u
				result.add(temp.getName().substring(0, temp.getName().length()-4));
			} 
		}

		Collections.sort(result); 
		return result;

	}

	
	/**
	 * return the title of every song in the playlist gived  
	 * @return ArrayList that contain the titles
	 * */
	
	public static ArrayList <String> getSongsString(String path, String playlist){

		ArrayList<String> songs=new ArrayList<String>();

		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(path+playlist+".m3u"));

			String line;
			while ((line = br.readLine()) != null) {
				if(!line.contains("#EXTINF") && !line.contains("#EXTM3U") && !line.isEmpty()){ //estrapolo titolo 
					//songs.add(getTitle(line));
					songs.add(line);

				}

			}
			br.close();

		}
		catch(Exception e){
			e.printStackTrace();
		}
		return songs;
	}




	/**
	 * create a playslist in the path 
	 * @return boolean that rappresent succes of the function
	 * */
	
	public static Boolean createPlaylist(String path,String titolo){

		try {
			File file = new File(path+titolo+".m3u");

			if (file.exists()){
				
				return false;
			}

			else if (file.createNewFile()){
				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("#EXTM3U\n"); 
				bw.close();

			}

			else{
				System.out.println("Il file " + path + " non può essere creato");
				return false;
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return true;

	}


	/**
	 * add a song in the playlist
	 * @return boolean that rappresent succes of the function
	 * */

	public static Boolean addSong(String Uri, String path, String playlistName) {


		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path+playlistName+".m3u", true)));
			out.println("#EXTINF:"+Id3Tag.getLength( Uri)+","+Id3Tag.getArtist(Uri)+" - "+Id3Tag.getTitle(Uri));
			out.println(Uri);
			out.flush();
			out.close();
		}catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;

	}





	/**
	 * remove song in the playlist 
	 * @return boolean that rappresent succes of the function
	 * */

	public static Boolean removeSong(int index_song,String path, String playlist){
		File inputFile = new File(path+playlist+".m3u");
		File tempFile = new File(path+playlist+"temp.m3u");

		BufferedReader reader;
		boolean successful = false;
		try {
			tempFile.createNewFile();
			reader = new BufferedReader(new FileReader(inputFile));

			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

			String currentLine;
			Boolean cond=false;
			int i=0;
			while((currentLine = reader.readLine()) != null) {

				if(currentLine.contains("#EXTINF")) {

					cond = false;

					if(i == index_song) {
						cond = true;
					}

					if (!cond) {
						writer.write(currentLine+"\n");
					}

					i++;
				}
				else {
					if (!cond) {
						writer.write(currentLine+"\n");
					}
				}
			}

			successful = tempFile.renameTo(inputFile);
			writer.flush();
			reader.close();
			writer.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
		}

		return successful;

	}


	
	/**
	 * give path of playlist and song's uri and remove it
	 * @return boolean that rappresent succes of the function
	 * */
	public static Boolean removeSong(String uri,String path, String playlist){
		File inputFile = new File(path+playlist+".m3u");
		File tempFile = new File(path+playlist+"temp.m3u");

		BufferedReader reader;
		boolean successful = false;
		try {
			tempFile.createNewFile();
			reader = new BufferedReader(new FileReader(inputFile));

			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

			String currentLine;
			String temp = "";
			while((currentLine = reader.readLine()) != null) {
				// trim newline when comparing with lineToRemove

				if(currentLine.contains("#EXTM3U")){
					writer.write(currentLine+"\n");
					continue;
				}
				else if(currentLine.contains("#EXTINF")){
					temp=currentLine;
					continue;
				}
				else if (currentLine.equals(uri) || currentLine.isEmpty()){ //nuovo file nella playlist
					continue;
				}
				else {
					writer.write(temp+"\n");
					writer.write(currentLine+"\n");
				}



			}
			writer.flush();

			successful = tempFile.renameTo(inputFile);
			reader.close();
			writer.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
		}

		return successful;

	}



	/**
	 * rename a playlist 
	 * @return Boolean
	 * */
	
	public static Boolean renamePlaylist(String path, String old, String nuovo){
		File inputFile = new File(path+old+".m3u");
		File tempFile = new File(path+nuovo+".m3u");

		if (!inputFile.exists()){
			
			return false;
		}
		else {
			return inputFile.renameTo(tempFile);
		}


	}

	public static boolean isthere(String path, String playlist){
		return new File(path+playlist+".m3u").exists();
	}

	public static Boolean removePlaylist(String path, String playlist){
		boolean cond=false;
		try {
			File file = new File(path+playlist+".m3u");
			cond=file.delete();

		} catch (Exception e) {
			e.printStackTrace();

		}

		return cond? true:false;


	}

/**
 * @return ArrayList containing every song in a folder 
 * */
	public static ArrayList<String> getDirectoryPlaylist(String path){
		ArrayList<String> audioFiles = new ArrayList<String>();

		if (path == null) return audioFiles;

		File f = new File(path);
		File[] listing = f.listFiles();
		Arrays.sort(listing);
		for(int i=0;i<listing.length;i++){
			File tmp = listing[i];
			//Directories are not allowed
			if(!tmp.isDirectory()){
				//Getting all the files extensions
				String[] name = tmp.getName().split("\\.");
				//If no extension, don't include
				if(name.length>=2 && !tmp.isHidden()){
					boolean ctrl = false;
					for(int j=0; j<formats.length; j++){
						if(name[name.length-1].equals(formats[j])){
							ctrl=true;
							j=formats.length;
						}
					}
					if(ctrl){
						try {
							audioFiles.add(tmp.getCanonicalPath());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return audioFiles;
	}

	

	/**
	 * private funztion that verify the consistence of url
	 * */

	private static boolean exists_uri(String URLName){
		try {
			HttpURLConnection.setFollowRedirects(false);

			HttpURLConnection con =
					(HttpURLConnection) new URL(URLName).openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	
}