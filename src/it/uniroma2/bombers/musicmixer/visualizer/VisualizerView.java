package it.uniroma2.bombers.musicmixer.visualizer;

import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.audiofx.Visualizer;
import android.util.AttributeSet;
import android.view.View;

public class VisualizerView extends View{
	
	private Visualizer visual;
	
	private byte[] fftStream;
	private byte[] waveStream;
	
	private Set<Renderer> rend;
	
	private Paint flashPaint = new Paint();
	private Paint fadePaint = new Paint();
	
	public Rect rect = new Rect();
	
	public boolean linked = false;

	public VisualizerView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialization();
	}
	
	public VisualizerView(Context context, AttributeSet attrs){
		this(context, attrs, 0);
	}
	
	public VisualizerView(Context context){
		this(context, null, 0);
	}
	
	private void initialization(){
		fftStream = null;
		waveStream = null;
		
		flashPaint.setColor(Color.argb(122, 255, 255, 255));
	    fadePaint.setColor(Color.argb(238, 255, 255, 255)); // Adjust alpha to change how quickly the image fades
	    fadePaint.setXfermode(new PorterDuffXfermode(Mode.MULTIPLY));
	    
	    rend = new HashSet<Renderer>();
	}
	
	/**
	 * Connect a media player to the graph drawer
	 * @param audioSessionId
	 */
	public void linkToPlayer(int audioSessionId){
		
		visual = new Visualizer(audioSessionId);
		visual.setEnabled(false);
		visual.setCaptureSize(Visualizer.getCaptureSizeRange()[0]);
		
		Visualizer.OnDataCaptureListener captListener = new Visualizer.OnDataCaptureListener()
	    {
	      @Override
	      public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes,
	          int samplingRate)
	      {
	        waveStream = bytes;
	        invalidate();
	      }

	      @Override
	      public void onFftDataCapture(Visualizer visualizer, byte[] bytes,
	          int samplingRate)
	      {
	        fftStream = bytes;
	        invalidate();
	      }
	    };
	    
	    visual.setDataCaptureListener(captListener, Visualizer.getMaxCaptureRate() / 2, true, true);
	    
	    visual.setEnabled(true);
	    linked = true;
	    
	}
	
	public void enable(){
		visual.setEnabled(true);
	}
	
	public void disable(){
		visual.setEnabled(false);
	}
	
	public void release(){
		visual.release();
		linked = false;
	}
	
	Bitmap bmp;
	Canvas cvs;
	
	private boolean flash = false;
	
	public void flash(){
		flash = true;
		invalidate();
	}
	
	public void addRenderer(Renderer renderer)
	{
		if(renderer != null)
		{
			rend.add(renderer);
		}
	}

	public void clearRenderers()
	{
		rend.clear();
	}
	
	@Override
	protected void onDraw(Canvas cvs){
		super.onDraw(cvs);
		
		rect.set(0, 0, getWidth(), getHeight());
		
		if(bmp==null){
			bmp = Bitmap.createBitmap(cvs.getWidth(), cvs.getHeight(), Config.ARGB_8888);
		}
		if(this.cvs==null){
			this.cvs = new Canvas(bmp);
		}
		
		if(waveStream!=null){
			WaveData wd = new WaveData(waveStream);
			for(Renderer r : rend){
				r.render(cvs, wd, rect);
			}
		}
		if(fftStream!=null){
			FFTData fftd = new FFTData(waveStream);
			for(Renderer r : rend){
				r.render(cvs, fftd, rect);
			}
		}
		
		cvs.drawPaint(fadePaint);
		
		if(flash==true){
			flash = false;
			cvs.drawPaint(flashPaint);
		}
		cvs.drawBitmap(bmp, new Matrix(), null);
	}

}
