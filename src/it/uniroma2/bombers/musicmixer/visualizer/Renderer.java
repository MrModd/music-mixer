package it.uniroma2.bombers.musicmixer.visualizer;

import android.graphics.Canvas;
import android.graphics.Rect;

public abstract class Renderer {

	protected float[] wavePoints;
	protected float[] fftPoints;
	
	Renderer(){}
	
	abstract public void onRender(Canvas cvs, WaveData wd, Rect rect);
	
	abstract public void onRender(Canvas cvs, FFTData fftd, Rect rect);
	
	// Collect the audio data and call onRender
	final public void render(Canvas cvs, WaveData wd, Rect rect){
		if (wavePoints == null || wavePoints.length < wd.data.length * 4) {
			wavePoints = new float[wd.data.length * 4];
		}

		onRender(cvs, wd, rect);
	}
	
	//Collect the fft data and call onRender
	final public void render(Canvas cvs, FFTData fftd, Rect rect){
		if (fftPoints == null || fftPoints.length < fftd.data.length * 4) {
			fftPoints = new float[fftd.data.length * 4];
		}

		onRender(cvs, fftd, rect);
	}

}
